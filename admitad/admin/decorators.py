from functools import wraps

from flask import g, redirect, url_for, request
from flask_login import login_required, current_user, current_app


def admin_required(func):
    @wraps(func)
    @login_required
    def decorated(*args, **kwargs):
        if current_user.is_authenticated:
            if getattr(current_user, 'is_admin'):
                return func(*args, **kwargs)
            else:
                return redirect(url_for('site.index'))
        return current_app.login_manager.unauthorized()
    return decorated


def settings_required(func):
    @wraps(func)
    def decorated(*args, **kwargs):
        view = url_for('admin.setup')
        # if request.path != view:
        #     if not g.settings or not g.settings.website_configured():
        #         return redirect(view)
        # elif g.settings and g.settings.website_configured():
        #     return redirect(url_for('admin.index'))
        return func(*args, **kwargs)
    return decorated