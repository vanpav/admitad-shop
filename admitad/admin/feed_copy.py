import re

from urllib.parse import quote

from .models import ProgramOffer
from admitad.modules.catalog.models import Offer, Shop, Brand, Filter


PARAMS_COLOR = ('цвет', 'colour', 'color', 'цвета')
PARAMS_SIZE = ('размер', 'size', 'размеры')
PARAMS_SEASON = ('сезон', 'сезонность',)
PARAMS_MATERIAL = ('материал верха', 'cover material')
PARAMS_VENDOR = ('производитель', 'страна-изготовитель', 'country of origin')
PARAMS_COLLECTION = ('коллекция',)


def copy(categories, program, keywords=False):
    if keywords:
        offers = ProgramOffer.objects(program_id=program.id)
    else:
        offers = ProgramOffer.objects(category_id__in=categories.keys())
    shop = Shop.get_or_create(program)

    current_offers_references = Offer\
        .objects(shop__id=shop.id).distinct('reference')
    added_offers_references = []

    brands = {}

    for offer in offers:
        converted = convert(offer, program_id=program.id)

        if keywords:
            category = None
        else:
            category = categories.get(offer.category_id)

        vendor_name = converted.pop('vendor')
        brand = brands.get(vendor_name, None)
        if not brand:
            brand = Brand.create_or_pass(vendor_name)
            brands[vendor_name] = brand
        try:
            real_offer, created = Offer.create_or_update(
                converted,
                shop=shop,
                brand=brand,
                category=category
            )

            if not real_offer.status.is_filled:
                facets = []
                params = converted.get('params', {})
                if PARAMS_COLOR[0] in params:
                    for color in params.get(PARAMS_COLOR[0]):
                        founded = Filter.objects(group__slug='color')\
                            .search_text('{}'.format(color)).order_by('$text_score')
                        facets += [f.facete for f in founded]

                if PARAMS_MATERIAL[0] in params:
                    for material in params.get(PARAMS_MATERIAL[0]):
                        founded = Filter.objects(group__slug='material')\
                            .search_text('{}'.format(material)).order_by('$text_score')
                        facets += [f.facete for f in founded]

                real_offer.update(set__facets=facets)

            added_offers_references.append(real_offer.reference)
        except Exception as e:
            print('Offer copy error: {}'.format(str(e)))

    should_set_not_active = list(set(current_offers_references) - set(added_offers_references))
    Offer.objects(reference__in=should_set_not_active).update(set__is_available=False)


def convert(offer, program_id=None):
    cls = CONVERTERS.get(program_id, Converter)
    converter = cls(offer)
    return converter.convert()


class BaseConverter:

    def __init__(self, obj):
        self.obj = obj

    def get_name(self):
        if self.obj.name:
            name = self.obj.name
        elif self.obj.model:
            name = self.obj.model
        return name.strip()

    def generate_reference(self):
        id = self.obj.gid if self.obj.gid else self.obj.oid
        return '-'.join([str(self.obj.program_id), id])

    def get_pictures(self):
        cls = Offer.pictures.field.document_type
        return [cls(url=self.urlquote(pic)) for pic in self.obj.pictures]

    def get_is_available(self):
        return self.obj.available

    def create_default(self):
        return {
            'name': self.get_name(),
            'description': self.obj.description,
            'url': self.obj.url,
            'price': self.obj.price,
            'old_price': self.obj.old_price,

            'is_available': self.get_is_available(),

            'pictures': self.get_pictures(),
            'vendor': self.obj.vendor.strip() if self.obj.vendor else None,

            'reference': self.generate_reference(),
            'params': self.get_params(),
        }

    @staticmethod
    def process_color(name, value):
        return PARAMS_COLOR[0], value

    @staticmethod
    def process_size(name, value):
        return PARAMS_SIZE[0], value

    @staticmethod
    def process_season(name, value):
        return PARAMS_SEASON[0], value

    @staticmethod
    def process_material(name, value):
        return PARAMS_MATERIAL[0], value

    @staticmethod
    def process_vendor(name, value):
        return PARAMS_VENDOR[0], value

    @staticmethod
    def process_collection(name, value):
        return PARAMS_COLLECTION[0], value

    def get_params(self):
        params = {}
        names = []
        for name, value in self.obj.params.items():
            name = name.strip().lower()
            names.append(name)
            param = None
            if name in PARAMS_COLOR:
                param = self.process_color(name, value)
            elif name in PARAMS_SIZE:
                param = self.process_size(name, value)
            elif name in PARAMS_SEASON:
                param = self.process_season(name, value)
            elif name in PARAMS_MATERIAL:
                param = self.process_material(name, value)
            elif name in PARAMS_VENDOR:
                param = self.process_vendor(name, value)
            elif name in PARAMS_COLLECTION:
                param = self.process_collection(name, value)
            if param:
                params[param[0]] = param[1]
        return params

    def convert(self):
        default = self.create_default()
        return default

    @staticmethod
    def urlquote(url):
        spl = url.split(':')
        quoted_url = ':'.join([spl[0],
                               quote(spl[1])])
        return quoted_url


class Converter(BaseConverter):
    pass


class LamodaConverter(BaseConverter):
    pass


class ConceptConverter(BaseConverter):
    def get_name(self):
        name = self.obj.model
        if name:
            return name.strip()
        return super(ConceptConverter, self).get_name()


class TomtailorConverter(BaseConverter):

    def get_name(self):
        name = super(TomtailorConverter, self).get_name()
        vendor_code = self.obj.extras.get('vendor_code', None)
        if vendor_code:
            r = '{}$'.format(vendor_code)
            return re.sub(r, '', name).strip()
        return name


class MexxConverter(TomtailorConverter):
    pass


class AlbaConverter(BaseConverter):
    def get_is_available(self):
        return True

    @staticmethod
    def process_size(name, value):
        if len(value) > 1:
            sizes = ','.join(value)
        else:
            sizes = value[0]
        sizes = list(set(size.strip() for size in sizes.split(',')))
        return PARAMS_SIZE[0], sizes


class BashmagConverter(BaseConverter):

    @staticmethod
    def process_size(name, value):
        if len(value) > 1:
            sizes = set(';'.join(value))
        else:
            sizes = value[0]
        sizes = list(set(size.strip() for size in sizes.split(';')))
        return PARAMS_SIZE[0], sizes


class WildberriesConverter(BaseConverter):

    @staticmethod
    def process_size(name, value):
        sizes = value[0].split('|')
        if len(sizes) and '/' in sizes[0]:
            sizes = [size.split('/')[0] for size in sizes]
        return PARAMS_SIZE[0], sizes


class AizelConverter(BashmagConverter):
    pass


class QuelleConverter(BaseConverter):

    @staticmethod
    def process_size(name, value):
        return AlbaConverter.process_size(name, value)


class RalfringerConverter(QuelleConverter):
    pass


class ButikConverter(BaseConverter):

    @staticmethod
    def process_size(name, value):
        params_size, sizes = AlbaConverter.process_size(name, value)
        try:
            sizes = [size.split()[0] for size in sizes]
        except:
            pass
        return params_size, sizes


class ShowroomsConverter(QuelleConverter):

    @staticmethod
    def process_size(name, value):
        params_size, sizes = AlbaConverter.process_size(name, value)
        return params_size, [size.replace('/', '.') for size in sizes]


class TheoutletConverter(BashmagConverter):
    pass


class BonprixConverter(QuelleConverter):
    pass


CONVERTERS = {
    1001:   LamodaConverter,
    2777:   TomtailorConverter,
    7065:   ConceptConverter,
    2775:   MexxConverter,
    4905:   BashmagConverter,
    4102:   WildberriesConverter,
    3447:   AlbaConverter,
    9221:   AizelConverter,
    1757:   ButikConverter,
    515:    QuelleConverter,
    14181:  RalfringerConverter,
    3761:   ShowroomsConverter,
    13685:  TheoutletConverter,
    14606:  BonprixConverter,
}