import os
import datetime
from urllib.parse import urljoin

from flask import url_for, render_template

from admitad.utils.filesys import (create_folders,
                                   get_full_path,
                                   delete_file_by_path)

from admitad.admin.models import AdminSettings
from admitad.modules.catalog.models import OffersIndex, Offer, Brand


SITEMAP_PATH = 'static/root/sitemap.xml'


def create_sitemap(sitemap_path, request):
    create_folders(os.path.split(sitemap_path)[0], True)

    time_format = '%Y-%m-%d'
    url_root = request.url_root
    settings = AdminSettings.get()

    last_update = settings.last_update.strftime(time_format)
    catalog_pages = [
        [urljoin(url_root, url_for('site.catalog', path=i.path)), last_update]
        for i in OffersIndex.objects(count__gt=0).order_by('+path')
        ]
    catalog_pages += [[urljoin(url_root, url_for('site.catalog', brand=brand.slug)), last_update]
                      for brand in Brand.objects()]
    catalog_pages = [[urljoin(url_root, url_for('site.catalog')), last_update]] + catalog_pages

    offer_pages = [
        [urljoin(url_root, url_for('site.offer', slug=o.slug)), o.updated_at.strftime(time_format)]
        for o in Offer.enabled()
    ]

    pages = [
        [urljoin(url_root, url_for('site.brands')), last_update],
        [urljoin(url_root, url_for('site.sale')), last_update]
    ]

    context = {
        'root': [url_root, datetime.datetime.now().strftime(time_format)],
        'catalog': catalog_pages,
        'offers': offer_pages,
        'pages': pages
    }

    sitemap = render_template('misc/sitemap.xml', **context)
    delete_file_by_path(sitemap_path)
    with open(get_full_path(sitemap_path), 'w') as f:
        f.write(sitemap)
    return sitemap