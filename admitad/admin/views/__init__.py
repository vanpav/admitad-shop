from .api import (get_task_result, toggle_program,
                  get_offers_examples, get_offer_example,
                  reupload_image, disable_offer, toggle_program_parse)

from .index import IndexView
from .setup import SetupView
from .program import ProgramsView, ProgramCategoriesView
from .catalog import (CatalogView, CatalogCategoryView,
                      CatalogBrandView, CatalogShopView,
                      CatalogFiltersView)

from .filler import (FillerView, )
from .pages import PagesView