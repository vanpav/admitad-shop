from flask import request, render_template, redirect, url_for, g

from .mixins import AdminViewMixin
from ..models import Program, ProgramCategory, ProgramOffer
from ..tasks import (update_programs, download_feed,
                     parse_categories, parse_offers, copy_offers)
from ..forms import ProgramCategoryForm

from admitad.modules.catalog.models import Category


class ProgramsView(AdminViewMixin):

    def get(self):
        programs = Program.objects()
        return render_template('admin/program/list.html', programs=programs)

    def post(self):
        res = None

        if not g.settings.has_tasks:

            if '_refresh' in request.form:
                res = update_programs.delay()

            if '_download' in request.form:
                res = download_feed.delay()

            if '_parse_categories' in request.form:
                res = parse_categories.delay()

            if '_parse_offers' in request.form:
                res = parse_offers.delay()

            if '_copy_offers' in request.form:
                res = copy_offers.delay()

            if res:
                g.settings.set_task(res)

        return redirect(url_for('admin.programs'))


class ProgramCategoriesView(AdminViewMixin):

    def get_context(self, pid, cid):
        program = Program.objects.get_or_404(id=pid)
        categories, current_category = program.get_categories_tree(cid)

        context = {
            'program': program,
            'categories': categories
        }

        offers = ProgramOffer.objects(program_id=pid)
        if cid:
            context['category'] = current_category
            form = ProgramCategoryForm(request.form,
                                       obj=context['category'])
            real_categories = Category.get_tree()
            form.copy_to.choices = form.copy_to.choices.copy() + \
                                   list(Category.flat_from_tree(real_categories))
            offers = offers.filter(category_id=str(context['category'].id))
            context['form'] = form

        page = request.args.get('page', 1)
        try:
            page = int(page)
        except ValueError:
            page = 1
        context['offers'] = offers.paginate(per_page=32, page=page)

        return context

    def get(self, pid, cid=None):
        context = self.get_context(pid, cid)

        return render_template('admin/program/item.html', **context)

    def post(self, pid, cid):
        context = self.get_context(pid, cid)
        form = context.get('form')
        category = context.get('category')

        if form.validate_on_submit():
            form.populate_obj(category)
            category.save()
            return redirect(url_for('admin.programs.item', pid=category.program_id))
