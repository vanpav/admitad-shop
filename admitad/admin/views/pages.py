from flask import render_template, request, redirect, url_for
from flask_wtf import Form

from wtforms import (validators as v, fields as f,
                     ValidationError, widgets as w,)

from .mixins import AdminViewMixin
from admitad.modules.catalog.models import Filter, Page


class PagesForm(Form):
    path = f.StringField('Путь', validators=[v.DataRequired()])

    title = f.StringField('Тайтл', validators=[v.Optional()])
    header = f.StringField('Заголовок H1', validators=[v.Optional()])
    description = f.StringField('Мета:описание', validators=[v.Optional()])

    content = f.TextAreaField('Текст', validators=[v.Optional()])

    def validate_path(self, field):
        if field.data:
            paths = [p.replace('_', ':') for p in field.data.split('/') if '_' in p]
            if not Filter.objects(facete__in=paths).count() == len(paths):
                raise ValidationError('Что то не так с путем!')


class PagesView(AdminViewMixin):

    def get_context(self, id):
        if id:
            page = Page.objects.get(id=id)
            form = PagesForm(request.form, obj=page)
        else:
            page = Page()
            form = PagesForm()
        return {
            'create': True if not id else False,
            'page': page,
            'form': form
        }

    def get(self, id=None):
        context = self.get_context(id)
        if request.endpoint == 'admin.pages.item.delete':
            context.get('page').delete()
            return redirect(url_for('admin.index'))
        return render_template('admin/catalog/pages.html',
                               **context)

    def post(self, id=None):
        context = self.get_context(id)
        form = context.get('form')
        if form.validate_on_submit():
            page = context.get('page')
            page.path = form.data.get('path')
            page.content = form.data.get('content')
            for attr in ('title', 'header', 'description'):
                setattr(page, '_{}'.format(attr), form.data.get(attr))
            page.save()
            return redirect(url_for('admin.pages.item', id=page.id))

        return redirect(url_for('admin.pages'))