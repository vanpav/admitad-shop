import random

from flask import render_template, request, redirect, url_for
from flask_login import current_user

from .mixins import AdminViewMixin

from ..models import UserFill
from admitad.modules.catalog.models import (Filter, FilterGroup, Offer)


class FillerView(AdminViewMixin):
    template = 'admin/filler/index.html'

    def get(self):
        groups = FilterGroup.get_dict()
        offer_id = request.args.get('id', None)

        context = {
            'groups': groups,
        }

        if not offer_id:
            ids = Offer.not_filled().distinct('id')
            not_filled_count = len(ids)
            offer_id = random.choice(ids)
            context.update(not_filled_count=not_filled_count)
        offer = Offer.objects.get(id=offer_id) if offer_id else None

        fill = UserFill.get(current_user.id)

        context.update(offer=offer, fill=fill)

        return render_template(self.template, **context)

    def post(self):
        form = dict(request.form)
        offer_id = form.pop('offer_id', None)[0]

        keys = list(form.keys())
        is_skipping = False
        if 'skip_offer' in keys:
            keys.pop(keys.index('skip_offer'))
            is_skipping = True

        if len(keys) and offer_id and not is_skipping:
            offer = Offer.objects.get(id=offer_id)
            filters = Filter.objects(id__in=[i for i in form.keys()])
            facets = [filter.make_facete() for filter in filters]

            if not offer.status.is_filled:
                fill = UserFill.create_or_update(current_user.id)
                fill.set_filled_offer(offer)

            offer.update(set__facets=facets,
                         set__status__is_filled=True)


        return redirect(url_for('admin.filler'))