from flask import render_template

from .mixins import AdminViewMixin

class IndexView(AdminViewMixin):
    template = 'admin/base.html'

    def get(self):
        return render_template(self.template)