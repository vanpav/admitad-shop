from flask.views import MethodView

from ..decorators import admin_required, settings_required


class AdminViewMixin(MethodView):
    decorators = [settings_required, admin_required]