from flask import request, redirect, render_template, url_for

from .mixins import AdminViewMixin
from ..forms import (AddCategoryForm, EditCategoryForm,
                     AddFilterGroupForm, AddFilterForm)

from admitad.modules.catalog.models import (Category, Offer,
                                            Shop, FilterGroup, Filter)

from admitad.modules.catalog.models.filters import EmbbedFilterGroup


class CatalogView(AdminViewMixin):

    def get(self, category_id=None):
        categories, current = Category.get_tree(category_id, True)
        offers = Offer.objects.only('name', 'url', 'pictures',
                                    'price', 'old_price', 'discount')

        filters = {}
        filters_groups = FilterGroup.objects.distinct('slug')
        match = []
        facets = {}

        if category_id:
            filters['category__path__istartswith'] = current.path

        for key, value in request.args.items():
            if key in ('shop', 'brand'):
                filters['{key}__slug'.format(key=key)] = value

            if key in filters_groups:
                if not filters.get('facets__all'):
                    filters['facets__all'] = []
                filters['facets__all'].append(':'.join([key, value]))


        if 'available' in request.args:
            filters['is_available'] = False


        # searches = {}
        # for f in offers.distinct('facets'):
        #     group, value = f.split(':')
        #     searches.setdefault(group, []).append(value)

        sort = request.args.get('sort')
        if sort:
            offers = offers.order_by(sort)

        page = request.args.get('page', 1)
        try:
            page = int(page)
        except ValueError:
            page = 1

        offers = offers.filter(**filters)
        filters = FilterGroup.get_dict(facets=list(filter(None, offers.distinct('facets'))))
        offers = offers.paginate(per_page=40, page=page)

        return render_template('admin/catalog/main.html',
                               categories=categories,
                               category=current,
                               offers=offers,
                               filters=filters,
                               shops=Shop.objects.only('name', 'slug'),
                               brands=[])


class CatalogCategoryView(AdminViewMixin):
    template = 'admin/catalog/categories.html'

    def get_context(self, category_id):
        categories = Category.get_tree()
        context = {}

        if not category_id:
            category = Category()
            form = AddCategoryForm(request.form)
            form.parent.choices = form.parent.choices.copy() + \
                                  [c for c in Category.flat_from_tree(categories)]
            context['categories'] = categories
        else:
            category = Category.objects.get_or_404(id=category_id)
            form = EditCategoryForm(request.form, obj=category)
            form.parent.choices = form.parent.choices.copy() + \
                                  [c for c in Category.flat_from_tree(categories,
                                                                      category_id)]

        context.update({'form': form,
                        'category': category})

        return context

    def get(self, category_id=None):
        context = self.get_context(category_id)

        if request.endpoint == 'admin.categories.delete':
            context.get('category').delete()
            return redirect(url_for('admin.categories'))

        return render_template(self.template, **context)

    def post(self, category_id=None):
        context = self.get_context(category_id)
        form = context.get('form')
        category = context.get('category')
        if form and form.validate_on_submit():
            form.populate_obj(category)
            category.save()
            return redirect(url_for('admin.categories'))
        return render_template(self.template, **context)


class CatalogFiltersView(AdminViewMixin):
    template = 'admin/catalog/filters.html'
    forms = {
        'group_form': AddFilterGroupForm,
        'filter_form': AddFilterForm,
    }

    def get_context(self, group_slug, id):
        context = {}

        groups = FilterGroup.objects

        context['groups'] = groups
        context['group_form'] = self.forms.get('group_form')(request.form)

        filter_form = self.forms.get('filter_form')(request.form)
        filter_form.group.choices = [(str(gr.id), gr.name,) for gr in groups]

        filters = Filter.objects

        if group_slug:
            current_group = groups.get_or_404(slug=group_slug)
            filter_form.group.data = str(current_group.id)
            filters = filters.filter(group__id=current_group.id)
            context['current_group'] = current_group

        context['filters'] = filters
        context['filter_form'] = filter_form

        return context

    def get(self, group_slug=None, id=None):
        context = self.get_context(group_slug, id)
        return render_template(self.template, **context)

    def post(self, group_slug=None, id=None):
        context = self.get_context(group_slug, id)
        group_form = context.get('group_form')
        filter_form = context.get('filter_form')

        if group_slug is None and group_form.validate_on_submit():
            group = FilterGroup()
            group_form.populate_obj(group)
            group.save()
            return redirect(url_for('admin.filters'))

        elif filter_form.validate_on_submit():
            current_group = context.get('current_group')

            filt = Filter()
            filter_form.populate_obj(filt)
            filt.group = EmbbedFilterGroup(**current_group.to_filter())
            filt.save()

            return redirect(url_for('admin.filters.group',
                                    group_slug=current_group.slug))

        return render_template(self.template, **context)


class CatalogShopView(AdminViewMixin):
    template = 'admin/catalog/shops.html'

    def get(self):
        return render_template(self.template)


class CatalogBrandView(AdminViewMixin):
    template = 'admin/catalog/brands.html'

    def get(self):
        return render_template(self.template)