from flask import render_template, request, redirect, url_for, g

from .mixins import AdminViewMixin
from ..admitad_client import get_client
from ..forms import SetupForm
from ..models import AdminSettings


class SetupView(AdminViewMixin):
    template = 'admin/setup.html'
    form = SetupForm

    def get(self):
        form = self.form(request.form)
        return render_template(self.template, form=form)

    def post(self):
        if '_delete' in request.form:
            return self.delete()
        form = self.form(request.form)
        if form.validate_on_submit():
            client = get_client()
            sites = client.Websites.get().get('results')
            site = None
            for item in sites:
                if str(item.get('id')) == form.website_id.data:
                    site = item
                    break

            if site:
                settings = g.settings
                if not settings:
                    settings = AdminSettings.create()

                settings.website_id = site.get('id')
                settings.website_name = site.get('name')
                settings.save()
                return redirect(url_for('admin.index'))

            else:
                form.errors['global'] = ['Сайт с таким ID не найден']

        return render_template(self.template, form=form)

    def delete(self):
        g.settings.delete()
        return redirect(url_for('admin.setup'))