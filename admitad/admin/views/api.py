from flask import request, jsonify, render_template

from admitad.ext import celery

from .mixins import admin_required
from ..models import Program, ProgramOffer
from ..feed_parse import parse
from ..tasks import download_image

from admitad.modules.catalog.models import Offer


@admin_required
def get_task_result():
    task_id = request.form.get('task_id')
    if task_id:
        return celery.AsyncResult(task_id).ready()
    return True


@admin_required
def toggle_program():
    pid = request.form.get('pid')
    assert pid and str(pid).isdigit()
    program = Program.get_by_id(pid)
    program.toogle_enabled()
    Offer.objects(reference__istartswith=str(program.id) + '-')\
         .update(set__is_enabled=program.is_enabled)
    return jsonify(result=True)


@admin_required
def toggle_program_parse():
    pid = request.form.get('pid')
    assert pid and str(pid).isdigit()
    program = Program.get_by_id(pid)
    if program.is_active and program.is_enabled:
        program.parse_by_keywords = not program.parse_by_keywords \
            if program.parse_by_keywords else True
    else:
        program.parse_by_keywords = False
    program.save()
    return jsonify(result=True)


@admin_required
def get_offers_examples():
    program_id = request.form.get('program_id')
    if not program_id:
        return
    program = Program.get_by_id(program_id)
    examples = parse(program.xml_file, adapter='offer', tag='offer',
                     parent='offers', live=True, limit=50,
                     cid=request.form.get('category_id', None))
    return jsonify(result=examples)


@admin_required
def get_offer_example():
    offer_id = request.form.get('offer_id')
    if not offer_id:
        return
    offer = ProgramOffer.objects.get(id=offer_id)
    html = render_template('admin/program/offer.html', offer=offer)
    return jsonify(result=html)


@admin_required
def reupload_image(offer_id):
    offer = Offer.objects(id=offer_id).first()
    if not offer:
        return
    download_image.delay(offer)
    return jsonify(result=True)


@admin_required
def disable_offer():
    offer_id = request.form.get('offer_id')
    if not offer_id:
        return