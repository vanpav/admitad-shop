from flask import Blueprint, g
from admitad.settings import Config
from .models import AdminSettings

admin = Blueprint('admin', __name__,
                  url_prefix=getattr(Config, 'ADMIN_URL_PREFIX', '/admin'),
                  template_folder='templates/')


from .views import *


admin.add_url_rule('/', view_func=IndexView.as_view('index'))
admin.add_url_rule('/123/', view_func=IndexView.as_view('index.uri'))

admin.add_url_rule('/setup/', view_func=SetupView.as_view('setup'))

# Programs
admin.add_url_rule('/programs/',
                   view_func=ProgramsView.as_view('programs'))
admin.add_url_rule('/programs/<int:pid>/',
                   view_func=ProgramCategoriesView.as_view('programs.item'))
admin.add_url_rule('/programs/<int:pid>/<cid>/',
                   view_func=ProgramCategoriesView.as_view('programs.item.view'))


# Catalog
admin.add_url_rule('/catalog/offers/',
                   view_func=CatalogView.as_view('catalog'))
admin.add_url_rule('/catalog/offers/<category_id>/',
                   view_func=CatalogView.as_view('catalog.category'))
admin.add_url_rule('/catalog/categories/',
                   view_func=CatalogCategoryView.as_view('categories'))
admin.add_url_rule('/catalog/categories/<category_id>/',
                   view_func=CatalogCategoryView.as_view('categories.edit'))
admin.add_url_rule('/catalog/categories/<category_id>/delete/',
                   view_func=CatalogCategoryView.as_view('categories.delete'))
admin.add_url_rule('/catalog/shops/',
                   view_func=CatalogShopView.as_view('shops'))
admin.add_url_rule('/catalog/brands/',
                   view_func=CatalogBrandView.as_view('brands'))
admin.add_url_rule('/catalog/filters/',
                   view_func=CatalogFiltersView.as_view('filters'))
admin.add_url_rule('/catalog/filters/<group_slug>/',
                   view_func=CatalogFiltersView.as_view('filters.group'))


# Filler
admin.add_url_rule('/filler/', view_func=FillerView.as_view('filler'))


# API
admin.add_url_rule('/api/get_task_result/',
                   view_func=get_task_result, methods=['POST'])
admin.add_url_rule('/api/toggle_program/',
                   view_func=toggle_program, methods=['POST'])
admin.add_url_rule('/api/toggle_program_parse/',
                   view_func=toggle_program_parse, methods=['POST'])
admin.add_url_rule('/api/get_offers_examples/',
                   view_func=get_offers_examples, methods=['POST'])
admin.add_url_rule('/api/get_offer_examples/',
                   view_func=get_offer_example, methods=['POST'])

admin.add_url_rule('/api/reupload_image/<offer_id>/',
                   view_func=reupload_image)

admin.add_url_rule('/api/disable_offer/<offer_id>/',
                   view_func=disable_offer)


# Pages
admin.add_url_rule('/pages/', view_func=PagesView.as_view('pages'))
admin.add_url_rule('/pages/create/', view_func=PagesView.as_view('pages.item.create'))
admin.add_url_rule('/pages/<id>/', view_func=PagesView.as_view('pages.item'))
admin.add_url_rule('/pages/<id>/delete/', view_func=PagesView.as_view('pages.item.delete'))


from flask import render_template
from .decorators import admin_required
from .models import UserFill, User, Offer

@admin.route('/stats/')
@admin_required
def users_stats():
    fills = UserFill.objects(stats__total__gt=0)
    user_ids = fills.distinct('user_id')
    users = {user.id: user for user in User.objects(id__in=user_ids)}
    return render_template('admin/user_stats.html',
                           fills=fills,
                           users=users,
                           total_offers=Offer.available().count())