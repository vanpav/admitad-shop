from flask_wtf import Form
from wtforms import (StringField, SubmitField, HiddenField,
                     BooleanField, SelectField, IntegerField,
                     validators as v, ValidationError)


class SetupForm(Form):
    website_id = StringField('ID сайта',
                             validators=[v.data_required('Нужно указать ID сайта в Адмитаде')])
    submit = SubmitField('Добавить')

    def validate_website_id(form, field):
        if not field.data.isdigit():
            raise ValidationError('Должны быть только цифры')


class ProgramCategoryForm(Form):
    copy = BooleanField('Копировать', validators=[v.Optional()])
    copy_to = SelectField('в категорию', default='__None',
                          choices=[('__None', 'Каталог')],
                          validators=[v.Optional()])
    submit = SubmitField('Сохранить')

    def validate_copy_to(form, field):
        if field.data == '__None':
            field.data = None


class AddCategoryForm(Form):
    name = StringField('Название', validators=[
        v.DataRequired('Название обязательно'),
        v.Length(min=3, max=200, message='Длина должна быть между %(min)d и %(max)d символами')
    ])
    slug = StringField('Ссылка', validators=[v.Optional()])
    parent = SelectField('Родитель', choices=[('__None', 'Нет родителя')],
                         default='__None', validators=[v.Optional()])

    def validate_parent(form, field):
        if field.data == '__None':
            field.data = None
        else:
            try:
                field.data = int(field.data)
            except ValueError:
                raise ValidationError('Неправильный выбор')


class EditCategoryForm(AddCategoryForm):
    position = IntegerField('Позиция', default=0, validators=[v.Optional()])


class AddFilterGroupForm(Form):
    name = StringField('Название', validators=[
        v.DataRequired('Название обязательно'),
        v.Length(min=3, max=100, message='Длина должна быть между %(min)d и %(max)d символами')
    ])
    slug = StringField('Ссылка', validators=[v.Optional()])


class AddFilterForm(Form):
    name = StringField('Название', validators=[
        v.DataRequired('Название обязательно'),
        v.Length(min=3, max=100, message='Длина должна быть между %(min)d и %(max)d символами')
    ])
    name_kind = StringField('«Какие»', validators=[
        v.Optional(),
        #v.DataRequired('Обязательно, нужно для заголовков'),
        v.Length(min=3, max=100, message='Длина должна быть между %(min)d и %(max)d символами')
    ])
    slug = StringField('Ссылка', validators=[v.Optional()])
    group = SelectField('Группа', choices=[], validators=[
        v.DataRequired('Группа обязательна')
    ])