from datetime import datetime, timedelta
from collections import OrderedDict

from pytils.translit import slugify

from admitad.ext import db
from admitad.modules.auth.models import User
from admitad.modules.catalog.models import Category, Offer


class AdminSettings(db.Document):
    website_id = db.IntField()
    website_name = db.StringField()
    last_update = db.DateTimeField()

    tasks = db.DictField()

    indexing = db.BooleanField(default=False)
    downloading = db.BooleanField(default=False)

    update_in = db.FloatField(default=1.00)

    def __str__(self):
        return '<{}>'.format(self.website_id)

    @classmethod
    def get(cls):
        return cls.objects.first()

    @classmethod
    def create(cls, **kwargs):
        settings = cls.get()
        if not settings:
            settings = cls(**kwargs)
            settings.save()
        return settings

    @property
    def should_update(self):
        return not self.has_tasks \
               and datetime.now() > self.last_update + timedelta(days=self.update_in)

    @property
    def has_tasks(self):
        return bool(self.tasks)

    def set_task(self, task):
        task_id = task.task_id if hasattr(task, 'task_id') else 'autoupdatetaskid'
        name = task.task_name if hasattr(task, 'task_name') else task.name
        update = {'set__tasks__{}'.format(name): task_id}
        self.update(**update)
        self.reload()

    def delete_task(self, name):
        try:
            tasks = self.tasks
            tasks.pop(name)
            self.update(set__tasks=tasks)
            self.reload()
        except KeyError:
            pass

    def updated(self):
        if self.website_configured():
            self.update(set__last_update=datetime.now())

    def website_configured(self):
        return self.website_id is not None

    def save(self, **kwargs):
        return super().save(**kwargs)


class Program(db.Document):
    id = db.IntField(primary_key=True)
    name = db.StringField(max_length=250)
    site_url = db.StringField()
    gotolink = db.StringField()
    image = db.StringField()
    csv_link = db.StringField()
    xml_link = db.StringField()
    csv_file = db.StringField()
    xml_file = db.StringField()
    adv_updated_at = db.DateTimeField()
    adm_updated_at = db.DateTimeField()
    last_updated_at = db.DateTimeField()
    parse_by_keywords = db.BooleanField(default=False)
    # last_parsed_at = db.DateTimeField()
    modified_at = db.DateTimeField()
    is_active = db.BooleanField(default=True)
    is_enabled = db.BooleanField(default=True)
    is_updating = db.BooleanField(default=False)

    folder = 'feeds'

    meta = {
        'ordering': ['id']
    }

    def __str__(self):
        return '<{id}:{name}>'.format(id=self.id,
                                      name=self.name)

    @classmethod
    def populate(cls, data):
        program = cls.objects(id=data.get('id')).first()
        if not program:
            program = cls(id=data.get('id'))
            program.name = data.get('name')
            program.site_url = data.get('site_url')
            program.image = data.get('image')

        program.is_active = data.get('status') == 'active' and \
                            data.get('connection_status') == 'active'
        if not program.is_active:
            program.is_enabled = False

        program.gotolink = data.get('gotolink')
        program.modified_at = datetime.strptime(data.get('modified_date'),
                                                '%Y-%m-%dT%H:%M:%S')

        xml_link = data.get('products_xml_link')
        feeds = cls.get_feed_info_by_url(xml_link, data.get('feeds_info', []))
        if not bool(feeds):
            feeds = {
                'xml_link': xml_link,
                'csv_link': data.get('products_csv_link')
            }

        for field, value in feeds.items():
            if getattr(program, field) != value:
                setattr(program, field, value)

        program.save()
        return program

    @classmethod
    def get_by_id(cls, id):
        return cls.objects(id=id).first()

    @db.queryset_manager
    def active(cls, qs):
        return qs.filter(is_active=True)

    @db.queryset_manager
    def enabled(cls, qs):
        return cls.active.filter(is_enabled=True)

    @property
    def feed_name(self):
        return '{id}-{name}'.format(id=self.id, name=slugify(self.name))

    @property
    def should_update_feed(self):
        if self.last_updated_at:
            return self.last_updated_at != self.adv_updated_at
        return True

    def set_xml_file(self, path):
        self.update(set__xml_file=path,
                    set__last_updated_at=self.adv_updated_at)
        self.reload()

    def toogle_enabled(self):
        self.is_enabled = not self.is_enabled
        self.save()

    def toggle_updating(self, value=None):
        if value is None:
            value = not self.is_updating
        self.update(set__is_updating=value)
        self.reload()

    def get_categories_tree(self, cid):
        categories = ProgramCategory.objects(program_id=self.id)
        c_dict = OrderedDict()
        current = None
        for item in categories:
            c_dict[item.cid] = {
                'item': item,
                'childrens': []
            }
            if cid and item.cid == cid:
                current = item

        tree = []
        for node in c_dict.values():
            if node['item'].pcid and node['item'].pcid in c_dict:
                c_dict[node['item'].pcid]['childrens'].append(node)
            else:
                tree.append(node)

        return sorted(tree, key=lambda x: x['item'].cid), current

    def clear_offers(self):
        ProgramOffer.objects(program_id=self.id).delete()

    def get_categories_for_parsing(self):
        to_parse = {cid: str(id) for cid, id
                   in ProgramCategory.objects(program_id=self.id,
                                              copy=True).values_list('cid', 'id')}
        return to_parse

    def get_categories_for_copy(self):
        to_copy = {}
        for category in ProgramCategory.objects(program_id=self.id,
                                                copy=True):
            to_copy[str(category.id)] = Category.objects(id=category.copy_to).first()
        return to_copy

    @staticmethod
    def get_feed_info_by_url(url, info):
        def strtodate(s):
            try:
                return datetime.strptime(s, '%Y-%m-%d %H:%M:%S')
            except ValueError:
                s, _ = s.rsplit('.', 1)
                return datetime.strptime(s, '%Y-%m-%d %H:%M:%S')

        result = {}
        for item in info:
            if item.get('xml_link') == url:
                result['xml_link'] = item.get('xml_link')
                result['csv_link'] = item.get('csv_link')
                result['adv_updated_at'] = strtodate(item.get('advertiser_last_update'))
                result['adm_updated_at'] = strtodate(item.get('admitad_last_update'))
                break
        return result


class ProgramCategory(db.Document):
    program_id = db.IntField(required=True)
    cid = db.StringField(max_length=80, required=True) # id
    pcid = db.StringField(max_length=80) # parentId
    name = db.StringField()
    copy = db.BooleanField(default=False)
    copy_to = db.StringField()
    offers_count = db.IntField(default=0)

    meta = {
        'ordering': ['+pcid'],
        'indexes': ['pcid',
                    'program_id',
                    ['pcid', 'program_id']]
    }

    def __str__(self):
        return '{pid}-{cid}:{name}'.format(pid=self.program_id,
                                           cid=self.cid,
                                           name=slugify(self.name))

    @classmethod
    def set_offers_count(cls, program_id, cid, value):
        instance = cls.objects(program_id=program_id,
                               cid=cid).first()
        if instance:
            instance.update(set__offers_count=value)

    @classmethod
    def create_or_update(cls, program_id, cid, **kwargs):
        category = cls.objects(program_id=program_id,
                               cid=cid).first()
        if not category:
            category = cls(program_id=program_id,
                           cid=cid, **kwargs)
        category.save()
        return category


class ProgramOffer(db.Document):
    program_id = db.IntField(required=True)
    category_id = db.StringField(default=None)

    available = db.BooleanField(default=True)
    enabled = db.BooleanField(default=True)

    oid = db.StringField(required=True, max_length=100)
    gid = db.StringField(max_length=100)

    name = db.StringField()
    model = db.StringField()
    type_prefix = db.StringField()
    description = db.StringField()
    vendor = db.StringField()
    url = db.StringField()

    price = db.FloatField(required=True)
    old_price = db.FloatField()

    pictures = db.ListField(db.StringField())

    params = db.DictField()

    extras = db.DictField()

    similiar = db.ListField(db.StringField())

    meta = {
        'indexes': [
            'program_id',
            'category_id',
            'available',
            ['program_id', 'available'],
            ['program_id', 'category_id', 'available'],
            ['program_id', 'gid'],
            ['program_id', 'oid']
        ],
        'ordering': ('+available',)
    }

    def update_params(self, **params):
        for param, value in params.items():
            self.params.setdefault(param, [])
            if value not in self.params[param]:
                self.params[param] = list(set(self.params[param] + value))

    @classmethod
    def create_or_update(cls, **kwargs):
        gid = kwargs.get('gid')
        if gid:
            obj = cls.objects(program_id=kwargs.get('program_id'),
                              gid=gid).first()
            if obj:
                obj.update_params(**kwargs.get('params'))
                obj.save()
                return obj

        obj = cls(**kwargs)
        obj.save()
        return obj


class UserFillOffer(db.EmbeddedDocument):
    item = db.ReferenceField(Offer)
    date = db.DateTimeField(default=datetime.now)

    meta = {
        'ordering': '-date'
    }


class UserFillStatsPayed(db.EmbeddedDocument):
    count = db.IntField(default=0)
    date = db.DateTimeField(default=datetime.now)


class UserFillStats(db.EmbeddedDocument):
    total = db.IntField(default=0)
    payed = db.ListField(db.EmbeddedDocumentField(UserFillStatsPayed))
    left = db.IntField(default=0)


class UserFill(db.Document):
    """
        юзер
        ставка рублей
        список заполненых офферов (оффер, время)
        статистика:
            всего заполнено
            за сколько уплачено (список дата + кол-во)
            за сколько не выплачено
    """
    user_id = db.ObjectIdField(required=True, unique=True)
    rate = db.FloatField(default=0.00)
    offers = db.ListField(db.EmbeddedDocumentField(UserFillOffer))
    stats = db.EmbeddedDocumentField(UserFillStats)

    @classmethod
    def create_or_update(cls, user_id):
        fill = cls.get(user_id)
        if not fill:
            fill = cls(user_id=user_id)
            fill.save()
        return fill

    @classmethod
    def get(cls, user_id):
        return cls.objects(user_id=user_id).first() or None

    def set_filled_offer(self, offer):
        self.update(
            push__offers=UserFillOffer(item=offer),
            inc__stats__total=1
        )
        self.reload()