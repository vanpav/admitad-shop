import os

from urllib import request
from urllib.error import HTTPError

from admitad.settings import Config

from flask_resize import generate_image


def get_full_path(path):
    return os.path.join(Config.BASE_DIR, path)


def make_dirs(path, relative=True):
    if relative:
        path = get_full_path(path)
    if not os.path.exists(path):
        os.makedirs(path)


def download_file(url, filename=None, folder='media', subfolder=None):

    if subfolder:
        make_dirs(os.path.join(folder, subfolder))
    else:
        make_dirs(folder)

    try:
        opener = request.urlopen(url)
    except (HTTPError, OSError) as e:
        print(e)
        return

    if opener.status != 200:
        return

    try:
        cd = opener.headers['Content-Disposition'].split(';')[1].strip().split('=')[1]
        fname, ext = os.path.splitext(cd)
    except (AttributeError, ValueError):
        fname, ext = os.path.splitext(url)

    if not filename:
        filename = fname

    file = filename + ext
    if subfolder:
        file = os.path.join(subfolder, file)

    relative_file = os.path.join(folder, file)
    local_file = get_full_path(relative_file)

    local_file, headers = request.urlretrieve(url, local_file)

    return (file, relative_file, local_file)


def resize_image(image, format='jpeg', width=500, height=750, suffix='big',
                 fill=1, quality=100, bgcolor='#ffffff'):
    folder, filename = os.path.split(image)
    image_path = os.path.join(Config.MEDIA_DIR, image)
    name, ext = os.path.splitext(filename)
    errors=False
    if format == 'jpeg':
        ext = '.jpg'
    elif format == 'png':
        ext = '.png'
    if suffix:
        new_filename = os.path.join(folder, '{}_{}{}'.format(name, suffix, ext))
    else:
        new_filename = os.path.join(folder, '{}{}'.format(name, ext))
    save_to_path = os.path.join(Config.MEDIA_DIR, new_filename)

    if os.path.exists(save_to_path):
        os.remove(save_to_path)

    try:
        generate_image(inpath=image_path, outpath=save_to_path,
                       format=format, width=width, height=height,
                       fill=fill, quality=quality, bgcolor=bgcolor)
    except:
        errors = True
    return new_filename, errors



