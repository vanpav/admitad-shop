from pyadmitad import api, items

from admitad.settings import Config


class AdmitadSettingsException(Exception):
    pass


def get_client():
    settings = getattr(Config, 'ADMITAD_SETTINGS')

    if not settings:
        raise AdmitadSettingsException('Admitad settings didn\'t set')

    return api.get_oauth_client_client(settings.get('client_id'),
                                       settings.get('client_secret'),
                                       settings.get('scopes'))




def check_programs(**kwargs):
    settings = getattr(Config, 'ADMITAD_SETTINGS')
    client = api.get_oauth_client_client(settings.get('client_id'),
                                         settings.get('client_secret'),
                                         'websites advcampaigns_for_website')

    campaigns = client.CampaignsForWebsite.get('198991', **kwargs).get('results')

    print(len(campaigns))