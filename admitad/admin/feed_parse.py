import html
import re
from collections import Counter

from flask import Markup
from lxml import etree

from .models import ProgramCategory, ProgramOffer
from .utils import get_full_path

from admitad.settings import Config


def _get_tag_text(tag, elem, default=None):
    tag = elem.find(tag)
    if tag is not None:
        return tag.text
    return default


def parse(file, adapter, tag, parent=None, **kwargs):
    cls = ADAPTERS.get(adapter)
    adapter = cls(file, tag=tag, **kwargs)
    try:
        result = adapter.iterate(tag, parent)
        return result
    except etree.LxmlError as e:
        print(e)


class FeedParseException(Exception):
    pass


class BaseAdapter:

    def __init__(self, file, tag, **kwargs):
        self.doc = etree.iterparse(get_full_path(file), events=('end',), recover=True, tag=tag)

    def iterate(self, tag, parent=None):
        for event, elem in self.doc:
            self.process(elem, tag)

            if parent and elem.tag == parent:
                elem.clear()
                break

    def process(self, element, tag, **kwargs):
        raise NotImplementedError('Method is not implemented')


class CategoryAdapter(BaseAdapter):
    model = ProgramCategory
    counter = None

    def __init__(self, file, tag, **kwargs):
        self.program_id = kwargs.pop('program_id', None)
        self.counter = Counter()
        if not self.program_id:
            raise FeedParseException('Program is not set')
        self.doc = etree.iterparse(get_full_path(file), events=('end',), recover=True)

    def iterate(self, tag, parent=None):
        super(CategoryAdapter, self).iterate(tag, parent)

        if hasattr(self, 'counter'):
            for cid, value in self.counter.items():
                self.model.set_offers_count(self.program_id,
                                            cid,
                                            value)

    def process(self, element, tag, **kwargs):

        if element.tag == tag:

            cid = element.get('id')
            assert cid is not None
            name = element.text
            pcid = element.get('parentId')

            if name and cid:
                name = name.strip()
                category = self.model.create_or_update(program_id=self.program_id,
                                                       cid=cid,
                                                       pcid=pcid,
                                                       name=name)

        if element.tag == 'offer':
            category_id = _get_tag_text('categoryId', element)
            if category_id:
                self.counter[category_id] += 1
            element.clear()



class OfferAdapter(BaseAdapter):

    def __init__(self, file, tag, **kwargs):
        self.result = []
        self.limit = kwargs.pop('limit', 30)
        self.live = kwargs.pop('live', False)
        if self.live:
            self.cid = kwargs.pop('cid')
        self.program = kwargs.pop('program', None)
        self.categories = kwargs.pop('categories', {})
        self.keywords = [kw.strip() for kw in Config.PARSING_KEYWORDS.split(',')] \
            if kwargs.pop('keywords', False) and hasattr(Config,'PARSING_KEYWORDS') else False
        super(OfferAdapter, self).__init__(file, tag, **kwargs)

    def iterate(self, tag, parent=None):
        for event, elem in self.doc:
            self.process(elem, tag)
            elem.clear()

            if self.live and self.limit == len(self.result):
                elem.clear()
                break

            if parent and elem.tag == parent:
                elem.clear()
                break

        if self.live:
            return ''.join(self.result)

    @staticmethod
    def _clear_key(key):
        denied_chars = '$.'
        return re.sub('[{}]'.format(denied_chars), ' ', key)

    def _collect_params(self, element):
        params = {}
        for e in element.findall('param'):
            key = e.get('name', None)
            value = e.text if e is not None else None
            if not key or not value:
                continue
            key = self._clear_key(key)
            params.setdefault(key, [])
            if value not in params[key]:
                params[key].append(value)
        return params

    def _from_elem(self, element):
        result = {
            'oid': element.get('id'),
            'gid': element.get('group_id', None),
            'available': element.get('available', 'true') == 'true'
        }

        for field, tag in (('name', 'name'),
                           ('model', 'model'),
                           ('type_prefix', 'typePrefix'),
                           ('description', 'description'),
                           ('vendor', 'vendor'),
                           ('url', 'url'),
                           ('price', 'price'),
                           ('old_price', 'oldprice')):
            result[field] = _get_tag_text(tag, element)

        result['pictures'] = [e.text for e in element.findall('picture')]

        result['params'] = self._collect_params(element)

        result['extras'] = {}
        for field, tag in (('vendor_code', 'vendorCode'),
                           ('article', 'article')):

            result['extras'][field] = _get_tag_text(tag, element)

        return result

    def process(self, element, tag, **kwargs):
        if element.tag == tag:
            if self.live:
                if self.cid and _get_tag_text('categoryId', element) != self.cid:
                     return
                string = etree.tostring(element, pretty_print=True).decode('utf-8')
                self.result.append(
                    Markup.escape(html.unescape(
                        string
                    ))
                )
            elif self.keywords and len(self.keywords):
                founded = False
                for field, tag in (('name', 'name'),
                                   ('model', 'model'),
                                   ('type_prefix', 'typePrefix')):
                    text = _get_tag_text(tag, element)
                    if text:
                        for kw in self.keywords:
                            if kw.lower() in text.lower():
                                founded = True
                                break
                    if founded:
                        break

                if founded:
                    result = self._from_elem(element)
                    result.update({
                        'program_id': self.program.id
                    })
                    offer = ProgramOffer.create_or_update(**result)
            else:
                category_id = _get_tag_text('categoryId', element)
                if category_id in self.categories:
                    result = self._from_elem(element)
                    result.update({
                        'category_id': self.categories.get(category_id),
                        'program_id': self.program.id
                    })
                    offer = ProgramOffer.create_or_update(**result)


ADAPTERS = {
    'category': CategoryAdapter,
    'offer': OfferAdapter
}
