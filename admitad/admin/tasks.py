import time

from celery import group as cgroup, chain as cchain

from pyadmitad.exceptions import ConnectionException, HttpException

from admitad.ext import celery

from .admitad_client import get_client
from .models import AdminSettings, Program
from .feed_parse import parse
from .feed_copy import copy

from .sitemap import create_sitemap as c_sitemap, SITEMAP_PATH

from admitad.settings import Config
from admitad.modules.catalog.models import OffersIndex, Offer, Filter, FilterGroup

from .utils import resize_image, download_file


OFFER_IMAGE_SIZES = (
    (200, 300, 'thumb'),
    (500, 750, 'medium'),
)


@celery.task(name='update_programs', bind=True)
def update_programs(self):
    settings = AdminSettings.get()

    try:
        client = get_client()
        campaigns = client.CampaignsForWebsite.get(
            settings.website_id,
            limit=100
        ).get('results')
        for campaign in campaigns:
            program = Program.populate(campaign)
    except (HttpException, ConnectionException) as e:
        print(e)

    settings.delete_task(self.name)
    settings.updated()


@celery.task(name='download_feed', bind=True)
def download_feed(self):
    settings = AdminSettings.get()
    programs = Program.enabled()
    for program in programs:
        if program.should_update_feed:
            try:
                print('Now downloading {}'.format(program.name))
                paths = download_file(program.xml_link,
                                      filename=program.feed_name,
                                      folder=program.folder)
                if paths:
                    program.set_xml_file(paths[1])
                else:
                    program.toogle_enabled()
            except Exception as e:
                print('{}: {}'.format(program.name, str(e)))

    settings.delete_task(self.name)


@celery.task()
def parse_program_category(program):
    if not program.xml_file:
        program.toogle_enabled()
        return

    program.toggle_updating(True)
    try:
        print('Now parsing {}'.format(program.name))
        categories = parse(program.xml_file,
                           adapter='category',
                           tag='category',
                           program_id=program.id)
    except Exception as e:
        print('{}: {}'.format(program.name, str(e)))

    program.toggle_updating(False)


@celery.task(name='parse_categories', bind=True)
def parse_categories(self):
    settings = AdminSettings.get()
    programs = Program.enabled()

    group = cgroup(parse_program_category.si(program) for program in programs)
    result = group.apply_async()

    while not result.ready():
        time.sleep(1)

    settings.delete_task(self.name)


@celery.task()
def parse_program_offer(program):

    if not program.xml_file:
        program.toogle_enabled()
        return

    if program.parse_by_keywords:
        options = {
            'program': program,
            'keywords': True
        }
    else:
        categories = program.get_categories_for_parsing()
        if not len(categories):
            return
        options = {
            'program': program,
            'categories':  categories
        }

    program.toggle_updating(True)
    try:
        program.clear_offers()
        print('Now parsing {}'.format(program.name))
        parse(program.xml_file,
              adapter='offer',
              tag='offer',
              parent='offers',
              **options)
    except Exception as e:
        print('{}: {}'.format(program.name, str(e)))

    program.toggle_updating(False)


@celery.task(name='parse_offers', bind=True)
def parse_offers(self):
    settings = AdminSettings.get()
    programs = Program.enabled()

    group = cgroup(parse_program_offer.si(program) for program in programs)
    result = group.apply_async()

    while not result.ready():
        time.sleep(1)

    settings.delete_task(self.name)


@celery.task()
def copy_program_offer(program):

    if program.parse_by_keywords:
        program.toggle_updating(True)
        print('Now copying {} via KEYWORDS'.format(program.name))
        copy(program=program, categories=None, keywords=True)
    else:
        categories = program.get_categories_for_copy()
        if not len(categories):
            return
        program.toggle_updating(True)
        print('Now copying {}'.format(program.name))
        copy(program=program, categories=categories)

    program.toggle_updating(False)


@celery.task(name='copy_offers', bind=True)
def copy_offers(self):
    settings = AdminSettings.get()
    programs = Program.enabled()

    group = cgroup(copy_program_offer.si(program) for program in programs)
    result = group.apply_async()

    while not result.ready():
        time.sleep(1)

    result = indexate_offers.delay()
    while not result.ready():
        time.sleep(1)

    create_sitemap.delay()
    download_images.delay()

    settings.delete_task(self.name)


@celery.task(name='download_image')
def download_image(offer):
    upd = {}
    try:
        file, relative, local_file = download_file(offer.pictures[0].url,
                                                   filename=offer.slug,
                                                   subfolder=str(offer.id))
        err = False
        upd['set__pictures__0__original'] = file
        for size in OFFER_IMAGE_SIZES:
            width, height, attr = size
            n, err = resize_image(file, width=width, height=height, suffix=attr)
            upd['set__pictures__0__{}'.format(attr)] = n

        if not err:
            upd['set__pictures__0__is_uploaded'] = True
            offer.update(**upd)
            offer.reload()
    except:
        return


@celery.task(name='download_images', bind=True)
def download_images(self):
    settings = AdminSettings.get()

    if settings.downloading:
        return

    offers = Offer.enabled().filter(pictures__0__is_uploaded=False)
    settings.update(set__downloading=True)
    for offer in offers:
        download_image(offer)
    settings.update(set__downloading=False)


@celery.task(name='indexate_offers', bind=True)
def indexate_offers(self):
    settings = AdminSettings.get()

    if settings.indexing:
        return

    settings.update(set__indexing=True)

    def make_paths():
        filters = Filter.get_grouped(facets=Offer.objects.distinct('facets'))

        def inner(slice, depth=1, parent=None):
            paths = []
            for group in slice:
                for filter in group:
                    if parent:
                        if filter.path == parent or filter.path.split('_')[0] == parent.split('_')[0]:
                            continue
                    path = '/'.join(sorted(parent.split('/') + [filter.path])) if parent else filter.path
                    paths.append(path)
                    if depth < Config.MAX_FILTERS:
                        paths = paths + inner(slice[1:], depth=depth + 1, parent=path)

            return paths

        return set(inner(filters))

    current_indexes_ids = OffersIndex.objects.distinct('id')
    new_indexes_ids = []

    for path in make_paths():
        index = OffersIndex.create_or_update(path)
        if index.count > 0:
            new_indexes_ids.append(index.id)

    should_delete = list(set(current_indexes_ids) - set(new_indexes_ids))

    OffersIndex.objects(id__in=should_delete).delete()
    FilterGroup.clear_cache()
    OffersIndex.update_cache()

    settings.update(set__indexing=False)


@celery.task(name='create_sitemap')
def create_sitemap():
    from admitad import app
    from flask import request

    with app.test_request_context():
        sitemap = c_sitemap(SITEMAP_PATH, request)
    print('SITEMAP GENERATED')


@celery.task(name='chain_update', bind=True)
def chain_update(self, settings=None):
    if not settings:
        settings = AdminSettings.get()

    settings.set_task(self)
    chain = cchain(update_programs.si(),
                   download_feed.si(),
                   parse_categories.si(),
                   parse_offers.si(),
                   copy_offers.si())
    res = chain.apply_async()

    while not res.ready():
        time.sleep(1)

    settings.updated()
    settings.delete_task(self.name)


@celery.task(name='autoupdate', bind=True)
def autoupdate(self):
    settings = AdminSettings.get()

    if settings.should_update:
        settings.set_task(self)
        res = chain_update.delay(settings=settings)

        while not res.ready():
            time.sleep(1)

        settings.delete_task(self.name)