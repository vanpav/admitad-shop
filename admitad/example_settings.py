import os
from datetime import timedelta

class Config:
    DEBUG = False
    SECRET_KEY = 'Keep secret'

    MAX_FILTERS = 2

    ADMIN_URL_PREFIX = '/admin'

    BASE_DIR = os.path.abspath(os.path.dirname(__file__))
    MEDIA_DIR = os.path.join(BASE_DIR, 'media')
    DATA_DIR = os.path.join(BASE_DIR, 'data')

    MONGODB_SETTINGS = {
        'db': 'admitad',
        'alias': 'default'
    }

    ADMITAD_SETTINGS = {
        'client_id': 'f0c690adfdb1ee81eae6ca791ef1a4',
        'client_secret': '84295ec95b6683ec5a41a3e236f400',
        'scopes': 'websites advcampaigns_for_website'
    }

    CACHE_TYPE = 'simple'
    CACHE_KEY_PREFIX = MONGODB_SETTINGS.get('db')

    CELERY_IMPORTS = {
        'admitad.admin.tasks'
    }
    CELERY_RESULT_BACKEND = "mongodb"
    CELERY_MONGODB_BACKEND_SETTINGS = {
        "host": 'localhost',
        "port": 27017,
        "database": '%s_celery' % MONGODB_SETTINGS.get('db')
    }

    CELERY_BROKER_URL = 'mongodb://localhost:27017/%s' % CELERY_MONGODB_BACKEND_SETTINGS.get('database')
    # CELERYBEAT_SCHEDULE = {
    #     'autoupdate': {
    #         'task': 'autoupdate',
    #         'schedule': timedelta(seconds=10)
    #     }
    # }

    SITE_NAME = 'Ботильоны'
    MINIFY_PAGE = True