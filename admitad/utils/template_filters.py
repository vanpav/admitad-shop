from flask import url_for

from admitad.settings import Config


def smart_round(value):
    val, flt = str(float(value)).split('.')
    return int(val)


def pretty_price(value):
    try:
        value = smart_round(value)
        html_string = '\u2009'
        return html_string.join('{:00,}'.format(value).split(','))
    except ValueError:
        pass
    return value


def make_offer_redirect_url(value):
    return url_for('site.offer_redirect', id=value)


def generate_filter_url(value, current=None, remove=False):
    max_filters = Config.MAX_FILTERS
    group, filt = value.split(':')
    filter_string = '_'.join([group, filt])

    if not current:
        return filter_string

    current_filters = [f for f in current.split('/') if not f.startswith(group)]

    if remove:
        all_current_filters = current.split('/')
        if filter_string in all_current_filters:
            all_current_filters.remove(filter_string)
            return None if len(all_current_filters) == 0 else \
                '/'.join(all_current_filters)

    if len(current_filters) >= max_filters:
        current_filters = current_filters[:-1]

    current_filters.append(filter_string)
    return '/'.join(sorted(current_filters))