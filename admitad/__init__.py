from .app import create_app
from .ext import celery

app = create_app()
