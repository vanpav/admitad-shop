from flask import Flask, g
from flask_login import current_user
from flask_mongoengine import MongoEngineSessionInterface

from werkzeug import SharedDataMiddleware

from pytils.dt import distance_of_time_in_words
from pytils.numeral import get_plural

from .settings import Config
from .ext import (db, login_manager, cache, celery, debug_toolbar, htmlmin)
from .web import site
from .admin import admin
from .modules.auth import User, auth

from .utils.template_filters import (pretty_price,
                                     generate_filter_url,
                                     make_offer_redirect_url)

from .admin.models import AdminSettings


def create_app(config='admitad.settings.Config'):
    app = Flask(__name__)
    app.config.from_object(config)

    configure_extensions(app)
    register_blueprints(app)
    add_context_processors(app)
    register_template_filters(app)

    app.session_interface = MongoEngineSessionInterface(db)
    app.jinja_env.add_extension('jinja2.ext.do')

    setup_login_manager(app)
    setup_admin_settings_context(app)

    app.add_url_rule('/media/<filename>', 'media',
                          build_only=True)
    app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
        '/media': app.config['MEDIA_DIR']
    })

    return app


def configure_extensions(app):
    for ext in (db, login_manager, celery, cache, htmlmin):
        ext.init_app(app)

    if app.debug:
        debug_toolbar.init_app(app)


def register_blueprints(app):
    for bp in (admin, site, auth):
        app.register_blueprint(bp)


def add_context_processors(app):
    pass


def register_template_filters(app):
    for filt in (distance_of_time_in_words,
                 generate_filter_url,
                 make_offer_redirect_url,
                 pretty_price,
                 get_plural):
        app.jinja_env.filters[filt.__name__] = filt


def setup_admin_settings_context(app):

    @app.before_request
    def check_settings():
        if current_user.is_authenticated and current_user.is_admin:
            settings = AdminSettings.get()
            g.settings = settings

    @app.context_processor
    def set_settings():
        return {'settings': g.settings if hasattr(g, 'settings') else None}


def setup_login_manager(app):
    login_manager.login_view = getattr(Config, 'LOGIN_VIEW', 'site.index')
    login_manager.login_message = None

    @login_manager.user_loader
    def load_user(user_id):
        return User.get_by_id(user_id)

    @app.before_request
    def set_last_seen():
        if current_user and current_user.is_authenticated:
            current_user.set_last_seen()