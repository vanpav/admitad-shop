from flask_mongoengine import MongoEngine
from flask_login import LoginManager
from flask_cache import Cache
from flask_debugtoolbar import DebugToolbarExtension
from flask_celery import Celery
from flask_htmlmin import HTMLMIN

db = MongoEngine()
login_manager = LoginManager()
cache = Cache()
celery = Celery()
debug_toolbar = DebugToolbarExtension()
htmlmin = HTMLMIN()
