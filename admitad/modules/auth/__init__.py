from flask import Blueprint
from .models import User

auth = Blueprint('auth', __name__, url_prefix='/auth')

from .views import *

auth.add_url_rule('/login/', view_func=login, methods=['GET', 'POST'], endpoint='login')
auth.add_url_rule('/logout/', view_func=logout, endpoint='logout')