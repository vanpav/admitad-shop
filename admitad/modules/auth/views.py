from flask import redirect, url_for, request, render_template
from flask_login import logout_user, login_required, current_user
from flask_wtf import Form
from wtforms import fields, validators as v

from .models import User


# def register():
#     user = User.create('vanpav@gmail.com', 'b1gfhjkm', is_admin=True)
#     return redirect(request.args.get('next') or url_for('site.index'))
#

class LoginForm(Form):
    email = fields.StringField('Логин', validators=[v.DataRequired(), v.Email()])
    password = fields.PasswordField('Пароль', validators=[v.DataRequired()])


def login():
    if current_user.is_authenticated:
        if current_user.is_admin:
            return redirect(request.args.get('next') or url_for('admin.index'))
        return redirect(request.args.get('next') or url_for('site.index'))

    form = LoginForm(request.form)
    context = {'form': form}

    if form.validate_on_submit():
        user = User.authenticate(form.email.data, form.password.data)
        if user:
            return redirect(request.args.get('next') or url_for('admin.index'))
        context['error'] = 'Неправильные данные для входа'

    return render_template('admin/auth/login.html', **context)


@login_required
def logout():
    logout_user()
    return redirect(url_for('admin.index'))