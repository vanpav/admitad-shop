from datetime import datetime
from flask_login import UserMixin, login_user
from werkzeug.security import generate_password_hash, check_password_hash

from admitad.ext import db


class User(UserMixin, db.Document):
    email = db.StringField(max_length=250, unique=True, required=True)
    is_admin = db.BooleanField(default=False)
    is_filler = db.BooleanField(default=False)
    last_seen = db.DateTimeField()
    created_at = db.DateTimeField(default=datetime.now())
    _password = db.StringField(required=True, min_length=6)

    meta = {
        'indexes': ['email']
    }

    def __str__(self):
        return '<{email}>'.format(email=self.email)

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def set_last_seen(self):
        self.update(set__last_seen=datetime.now())

    @classmethod
    def get_by_email(cls, email):
        return cls.objects(email=email).first()

    @classmethod
    def get_by_id(cls, user_id):
        return cls.objects.get(id=user_id)

    @classmethod
    def create(cls, email, password, is_admin=False, is_filler=False):
        user = cls(email=email, is_admin=is_admin, is_filler=is_filler)
        user.password = password
        user.save()
        return user

    @classmethod
    def authenticate(cls, email, password, remember=False):
        user = cls.get_by_email(email)
        if user and user.check_password(password):
            login_user(user, remember=remember)
            return user


