import random

from urllib.parse import urljoin

from flask import session, url_for, request
from mongoengine import signals
from werkzeug.utils import cached_property

from admitad.ext import db, cache

from .mixins import IDMixin, MetaDataMixin, SlugMixin, DateStampsMixin


class OfferPictures(db.EmbeddedDocument):
    url = db.URLField(required=True)
    original = db.StringField()
    medium = db.StringField()
    thumb = db.StringField()
    is_uploaded = db.BooleanField(default=False)


class OfferShop(db.EmbeddedDocument):
    id = db.ObjectIdField(required=True)
    name = db.StringField()
    logotype = db.StringField()
    slug = db.StringField()


class OfferCategory(db.EmbeddedDocument):
    id = db.IntField(required=True)
    name = db.StringField(max_length=250)
    path = db.StringField()


class OfferBrand(db.EmbeddedDocument):
    id = db.ObjectIdField(required=True)
    name = db.StringField(max_length=250)
    slug = db.StringField()


class OfferStatus(db.EmbeddedDocument):
    is_skipped = db.BooleanField(default=False)
    is_filled = db.BooleanField(default=False)


class OfferStats(db.EmbeddedDocument):
    views = db.IntField(default=0)
    redirects = db.IntField(default=0)
    favorites = db.IntField(default=0)


class Offer(SlugMixin, MetaDataMixin, DateStampsMixin, db.Document):

    slug = db.StringField(max_length=250, unique=True)

    name = db.StringField(max_length=250, required=True)
    url = db.URLField()
    description = db.StringField()

    price = db.FloatField(required=True)
    old_price = db.FloatField()
    discount = db.IntField(default=0)

    shop = db.EmbeddedDocumentField(OfferShop)
    category = db.EmbeddedDocumentField(OfferCategory)
    brand = db.EmbeddedDocumentField(OfferBrand)

    is_available = db.BooleanField(default=True)
    is_enabled = db.BooleanField(default=True)

    pictures = db.ListField(db.EmbeddedDocumentField(OfferPictures))
    params = db.DictField()

    reference = db.StringField()
    facets = db.ListField(db.StringField())

    status = db.EmbeddedDocumentField(OfferStatus)

    meta = {
        'index_background': True,
        'indexes': [
            'reference',
            'brand.id',
            'shop.id',
            'category.id',
            'price',

            ['facets'],
            ['category.id'],
            ['is_available', 'is_enabled'],
            ['facets', 'is_available', 'is_enabled'],
            ['category.id', 'facets']
        ],
    }

    @db.queryset_manager
    def available(cls, qs):
        return cls.enabled().filter(is_available=True)

    @db.queryset_manager
    def enabled(cls, qs):
        return qs.filter(is_enabled=True)

    @db.queryset_manager
    def not_filled(cls, qs):
        return cls.available().filter(status__is_filled__ne=True)

    @classmethod
    def create_or_update(cls, obj, shop, category=None, brand=None):
        reference = obj.get('reference')
        offer = cls.objects(reference=reference).first()
        if not offer:
            offer = cls(shop=shop.to_offer() if shop else None,
                        category=category.to_offer() if category else None,
                        brand=brand.to_offer() if brand else None,
                        **obj)
            offer.status = OfferStatus(is_filled=False, is_skipped=False)
            offer.save()
            return offer, True
        else:
            has_updates = False
            for attr in ('price', 'old_price', 'url', 'is_available'):
                if hasattr(offer, attr) and getattr(offer, attr) != obj.get(attr):
                    has_updates = True
                    setattr(offer, attr, obj.get(attr, None))
            if len(offer.pictures):
                if not offer.pictures[0].is_uploaded:
                    has_updates = True
                    offer.pictures = obj.get('pictures')
            if has_updates:
                offer.save()
            offer.update(
                set__params=obj.get('params'),
                set__is_available=obj.get('is_available')
            )
            return offer, False

    def __str__(self):
        return '{}'.format(self.slug)

    def __repr__(self):
        return '{}'.format(self.slug)

    def validate_slug(self, populate_from='name'):
        if not self.id:
            random_list = random.sample(range(10000000, 90000000), 50)
            salt = random.randint(10, 100)
            random_id = ''.join([str(random.choice(random_list)), str(salt)]) \
                if salt % 2 == 0 else ''.join([str(salt), str(random.choice(random_list))])
            string_id = format(random.choice(range(1, 9)), '0{}'.format(2))[::-1] + random_id
            super(Offer, self).validate_slug(populate_from)
            slug = self.slug
        else:
            string_id, slug = self.slug.split('-', 1)
        self.slug = '-'.join([string_id, slug])

    def calculate_discount(self):
        if self.old_price and self.old_price > 0:
            self.discount = round(
                (1 - self.price / self.old_price) * 100
            )

    @cached_property
    def in_favorites(self):
        return str(self.id) in session.get('favorites', [])

    def get_picture(self, size='medium'):
        try:
            pic = self.pictures[0]
            sized_picture = getattr(pic, size)
            if hasattr(pic, size) and sized_picture:
                return url_for('media', filename=sized_picture)
            return pic.url
        except IndexError:
            return

    @cached_property
    def get_sizes(self):
        sizes = self.params.pop('размер', None)
        if not sizes:
            return
        return sorted(sizes)

    @cached_property
    def picture(self):
        return self.get_picture('medium')

    @cached_property
    def small_picture(self):
        return self.get_picture('thumb')

    @property
    def picture_absolute_path(self):
        return urljoin(request.url_root, self.picture)

    def save(self, **kwargs):
        self.validate_slug()
        self.calculate_discount()
        return super(Offer, self).save(**kwargs)