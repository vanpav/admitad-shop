from admitad.ext import db, cache
from .offer import Offer


class OffersIndex(db.Document):
    path = db.StringField(required=True, unique=True)
    count = db.IntField(default=0)

    meta = {
        'indexes': [
            'path'
        ]
    }

    @classmethod
    def create_or_update(cls, path):

        facets = [p.replace('_', ':') for p in path.split('/')]
        index = cls.objects(path=path).first()

        count = Offer.available().filter(facets__all=facets).count()

        if index:
            if index.count != count:
                index.update(set__count=count)
                index.reload()
        else:
            index = cls(path=path, count=count)
            index.save()

        return index


    @classmethod
    @cache.memoize(60 * 60)
    def get_dict(cls):
        return {o.path: o.count for o in cls.objects}

    @classmethod
    def get_dict_by_paths(cls, paths):
        return {o.path: o.count for o in cls.objects(path__in=paths)}


    @classmethod
    def update_cache(cls):
        cls.clear_cache()
        cls.get_dict()

    @classmethod
    def clear_cache(cls):
        cache.delete_memoized(cls.get_dict)