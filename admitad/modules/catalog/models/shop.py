from mongoengine import signals

from admitad.ext import db

from .mixins import MetaDataMixin, SlugMixin
from .offer import Offer


class Shop(SlugMixin, MetaDataMixin, db.Document):
    program_id = db.IntField(required=True)
    name = db.StringField(max_length=150, required=True)
    site = db.URLField()
    link = db.URLField()
    logotype = db.StringField()
    is_enabled = db.BooleanField(default=True)

    meta = {
        'ordering': ['+name'],
        'indexes': [
            'slug',
            ['slug', 'is_enabled']
        ]
    }

    def __str__(self):
        return '{}:{}'.format(self.id, self.slug)

    @classmethod
    def get_or_create(cls, program):
        instance = cls.objects(program_id=program.id).first()
        if not instance:
            instance = cls(program_id=program.id,
                           name=program.name,
                           logotype=program.image,
                           is_enabled=program.is_enabled)
        instance.site = program.site_url
        instance.link = program.gotolink
        instance.save()
        return instance

    @classmethod
    def pre_delete(cls, sender, document, **kwargs ):
        Offer.objects(shop__id=document.id)\
            .update(set__shop=None)

    def update_offers(self):
        if self and self.id:
            Offer.objects(shop__id=str(self.id))\
                 .update(set__shop=self.to_offer())

    def to_offer(self):
        return {
            'id': self.id,
            'name': self.name,
            'logotype': self.logotype,
            'slug': self.slug
        }

    def make_facete(self):
        return ':'.join([self.__class__.__name__.lower(),
                         self.slug])

    def save(self, **kwargs):
        self.validate_slug()
        self.update_offers()
        return super(Shop, self).save(**kwargs)

signals.pre_delete.connect(Shop.pre_delete, sender=Shop)