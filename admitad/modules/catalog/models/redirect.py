from admitad.ext import db


class CatalogRedirect(db.Document):
    path_from = db.StringField(required=True, unique=True)
    path_to = db.StringField(required=True, unique=True)

    meta = {
        'indexes': [
            'path_from'
        ]
    }

    def __str__(self):
        return ' -> '.join([self.path_from, self.path_to])

    @classmethod
    def create_or_update(cls, path_from, path_to):
        redirect = cls.objects(path_from=path_from).first()
        if not redirect:
            redirect = cls.objects(path_to=path_to).first()
        if redirect:
            if path_to != redirect.path_to:
                redirect.update(set__path_to=path_to)
                redirect.reload()
            elif path_from != redirect.path_from:
                redirect.update(set__path_from=path_from)
                redirect.reload()
        else:
            redirect = cls(path_from=path_from, path_to=path_to).save()
        return redirect

    @classmethod
    def get_path(cls, path_from):
        redirect = cls.objects(path_from=path_from).first()
        if redirect:
            return redirect.path_to
        return