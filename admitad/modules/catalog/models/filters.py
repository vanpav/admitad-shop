from collections import OrderedDict
from werkzeug.utils import cached_property

from admitad.ext import db, cache
from admitad.utils.template_filters import generate_filter_url

from .index import OffersIndex
from .mixins import IDMixin, SlugMixin


class FilterGroup(IDMixin, SlugMixin, db.Document):
    name = db.StringField(max_length=100, required=True)
    position = db.IntField(default=None)

    meta = {
        'ordering': ['+position']
    }

    @classmethod
    @cache.memoize(60*60)
    def get_dict(cls, facets=None, path=None):
        groups = {g.id: {'slug': g.slug, 'name': g.name, 'position': g.position}
                  for g in cls.objects}
        groups = OrderedDict(sorted(groups.items(), key=lambda x: x[1]['position']))
        filters = Filter.objects

        if isinstance(facets, list):
            filters = filters.filter(facete__in=facets)
            if not filters.count():
                return {}

        indexes_paths = []

        for filter in filters:
            if filter.group.id in groups:
                if not groups[filter.group.id].get('filters'):
                    groups[filter.group.id]['filters'] = []
                f = MappedFilter(name=filter.name,
                                 slug=filter.slug,
                                 path=generate_filter_url(filter.facete, path),
                                 fid=filter.id,
                                 facete=filter.facete)
                groups[filter.group.id]['filters'].append(f)
                indexes_paths.append(f.path)

        indexes = OffersIndex.get_dict_by_paths(indexes_paths)

        for _, group in groups.items():
            for filter in group.get('filters', []):
                filter.count = indexes.get(filter.path, False)

        return groups

    def set_position(self, position=None):
        if position is None:
            last = self.__class__.objects.order_by('-position').first()
            position = last.position + 1 if last else 0
        assert isinstance(position, int)
        if self.position is None:
            self.position = position

    def to_filter(self):
        return {
            'id': self.id,
            'name': self.name,
            'slug': self.slug
        }

    @classmethod
    def clear_cache(cls):
        cache.delete_memoized(cls.get_dict)

    def save(self, **kwargs):
        self.validate_slug()
        self.set_position()
        super(FilterGroup, self).save(**kwargs)


class MappedFilter:
    count = False

    def __init__(self, name, slug, fid, facete, path=None):
        self.name = name
        self.slug = slug
        self.path = path
        self.id = fid
        self.facete = facete


class EmbbedFilterGroup(db.EmbeddedDocument):
    id = db.IntField(required=True)
    name = db.StringField(required=True)
    slug = db.StringField(required=True)


class Filter(IDMixin, SlugMixin, db.Document):
    name = db.StringField(max_length=100, required=True)
    name_kind = db.StringField(max_length=100) # какие?
    slug = db.StringField(required=True, unique_with='group.id')
    group = db.EmbeddedDocumentField(EmbbedFilterGroup, required=True)
    facete = db.StringField(max_length=60)
    extras = db.DynamicField()

    meta = {
        'ordering': ['name'],
        'indexes': [
            ['slug'],
            ['name'],
            ['facete'],
            ['group.slug'],
            {
                'fields': ['$name'],
                'default_language': 'russian',
                'weights': {'name': 10}
            }
        ]
    }

    def __str__(self):
        return '{}'.format(self.facete)

    @cached_property
    def path(self):
        return self.facete.replace(':', '_')

    def make_facete(self):
        return ':'.join([self.group.slug,
                         self.slug])

    def make_name_kind(self):
        if not self.name_kind:
            self.name_kind = self.name

    @classmethod
    def get_grouped(cls, facets=None):
        grouped = {}
        options = {}
        if facets and isinstance(facets, list):
            options['facete__in'] = facets
        for filter in cls.objects(**options):
            grouped.setdefault(filter.group.id, []).append(filter)
        return list(grouped.values())

    def save(self, **kwargs):
        self.validate_slug()
        self.facete = self.make_facete()
        self.make_name_kind()
        super(Filter, self).save(**kwargs)


