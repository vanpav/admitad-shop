from pytils.translit import slugify
from mongoengine import signals

from admitad.ext import db

from .mixins import IDMixin, MetaDataMixin, SlugMixin
from .offer import Offer


class Brand(SlugMixin, MetaDataMixin, db.Document):
    name = db.StringField(max_length=250, required=True)
    logotype = db.StringField()
    merged_with = db.ReferenceField('Brand')

    add_zeroes_to_id = 4

    meta = {
        'ordering': ['name'],
        'indexes': [
            'id',
            'name',
            'slug',
            ['id', 'name'],
        ]
    }

    def __str__(self):
        return '{}:{}'.format(self.id, self.slug)

    @classmethod
    def create_or_pass(cls, name):
        if name is None:
            return
        slug = slugify(slugify(name))
        obj = cls.objects(slug=slug).first()
        if not obj:
            obj = cls(name=name)
            obj.save()
        return obj

    @classmethod
    def pre_delete(cls, sender, document, **kwargs ):
        Offer.objects(brand__id=document.id)\
            .update(set__brand=None)

    def update_offers(self):
        if self and self.id:
            Offer.objects(brand__id=self.id)\
                .update(set__brand=self.to_offer())

    def to_offer(self):
        return {
            'id': self.id,
            'name': self.name,
            'slug': self.slug
        }

    def make_facete(self):
        return ':'.join([self.__class__.__name__.lower(),
                         self.slug])

    def save(self, **kwargs):
        self.validate_slug()
        self.update_offers()
        return super(Brand, self).save(**kwargs)

signals.pre_delete.connect(Brand.pre_delete, sender=Brand)