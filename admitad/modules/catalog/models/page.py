from admitad.ext import db
from .filters import Filter


class Page(db.Document):
    path = db.StringField(unique=True)
    # filters = db.ListField(db.ReferenceField(Filter))

    _title = db.StringField(max_length=500)
    _header = db.StringField(max_length=500)
    _description = db.StringField(max_length=750)

    content = db.StringField(default='')

    meta = {
        'indexes': [
            'path'
        ]
    }

    def __str__(self):
        return '<{}>'.format(self.path)

    @property
    def title(self):
        return self._title

    @property
    def header(self):
        return self._header

    @property
    def description(self):
        return self._description