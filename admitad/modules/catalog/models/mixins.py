from datetime import datetime

from pytils.translit import slugify
from mongoengine import signals

from admitad.ext import db


class DateStampsMixin:
    updated_at = db.DateTimeField()
    created_at = db.DateTimeField(default=datetime.now)

    def save(self, **kwargs):
        self.updated_at = datetime.now()
        return super(DateStampsMixin, self).save(**kwargs)


class IDMixin:
    id = db.IntField(primary_key=True)

    def _set_id(self):
        last_obj = self.__class__.objects.order_by('-id').only('id').first()
        id = last_obj.id + 1 if last_obj else 1
        self.id = id

    def save(self, **kwargs):
        if not self.id:
            self._set_id()
        return super(IDMixin, self).save(**kwargs)


class MetaData(db.EmbeddedDocument):
    title = db.StringField()
    description = db.StringField()
    keywords = db.StringField()


class MetaDataMixin:
    metas = db.EmbeddedDocumentField(MetaData)

    def save(self, **kwargs):
        if (not self.metas or not self.metas.title) and hasattr(self, 'name'):
            self.metas = MetaData(title=self.name)
        super(MetaDataMixin, self).save(**kwargs)


class SlugMixin:
    slug = db.StringField(max_length=250)

    def validate_slug(self, populate_from='name'):
        if not self.slug:
            self.slug = getattr(self, populate_from)
        self.slug = slugify(slugify(self.slug.strip()))


class SlugField(db.StringField):

    def __init__(self, populate_from, **kwargs):
        assert hasattr(self, populate_from)
        self.populate_from = populate_from
        super(SlugField, self).__init__(**kwargs)

    def on_document_pre_save(self, sender, document, **kwargs):
        slug = document._data.get(self.name)
        if not slug:
            slug = document._data.get(self.populate_from)
        document._data[self.name] = slugify(slug)

    def __get__(self, instance, owner):
        # print(instance._data.get(self.populate_from))
        if not hasattr(self, 'owner'):
            self.owner = owner
            signals.pre_save.connect(self.on_document_pre_save, sender=owner)

        return super(SlugField, self).__get__(instance, owner)