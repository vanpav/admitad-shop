from pytils.translit import slugify
from mongoengine import signals

from admitad.ext import db

from .mixins import IDMixin, DateStampsMixin, SlugMixin
from .offer import Offer


class Category(IDMixin, SlugMixin, DateStampsMixin, db.Document):
    name = db.StringField(max_length=200, required=True)
    path = db.StringField(unique=True)
    position = db.IntField()
    depth = db.IntField(default=0)
    parent = db.IntField(default=None)

    meta = {
        'ordering': ['position'],
        'indexes': ['path',
                    'parent',
                    'position',
                    ['path', 'position']]
    }

    def __str__(self):
        return '{}-{}'.format(self.id, self.slug)

    @classmethod
    def get_tree(cls, category_id=None, with_current=False):
        categories = cls.objects
        flat = {item.id: {'item': item, 'childrens': []}
                for item in categories}
        tree = []
        current = None
        for node in flat.values():
            if node['item'].parent and node['item'].parent in flat:
                flat[node['item'].parent]['childrens'].append(node)
                flat[node['item'].parent]['childrens']\
                    .sort(key=lambda x: x['item'].position)
            else:
                tree.append(node)
            if category_id and int(category_id) == node['item'].id:
                current = node['item']
        tree.sort(key=lambda x: x['item'].position)
        if with_current:
            return tree, current
        return tree

    @classmethod
    def pre_delete(cls, sender, document, **kwargs):
        childs = document.get_childs()
        for child in childs:
            if child.parent == document.id:
                child.parent = None
            child.save()

        Offer.objects(category__id=document.id)\
            .update(set__category=None)

    @classmethod
    def post_save(cls, sender, document, **kwargs):
        childs = cls.objects(parent=document.id)
        for child in childs:
            child.save()

        Offer.objects(category__id=document.id)\
            .update(set__category=document.to_offer())

    def get_childs(self):
        return self.__class__.objects(
            __raw__={'path': {'$regex': '^{}'.format(self.path)}}
        ).order_by('path')

    def get_childs_ids(self):
        return [child.path for child in self.get_childs()]
        # return [child.id for child in self.get_childs()]
        # return [child.make_facete() for child in self.get_childs()]

    def _check_path_exists(self):
        filters = dict(path=self.path)
        if self.id:
            filters['id__ne'] = self.id

        return self.__class__.objects(**filters).count()

    def get_parent(self):
        return self.__class__.objects(id=self.parent).first()

    def create_path(self, parent):
        suffix = 1

        if parent and parent != self:
            self.path = '/'.join([parent.path, self.slug])
        else:
            self.path = self.slug

        while self._check_path_exists():
            self.path = '-'.join([self.path, str(suffix)])
            suffix += 1

    def set_depth(self, parent):
        self.depth = parent.depth + 1 if parent else 0

    def set_position(self, position=None):
        if position is None:
            last = self.__class__.objects(parent=self.parent)\
                .order_by('-position').first()
            position = last.position + 1 if last else 0
        assert isinstance(position, int)
        if self.position is None:
            self.position = position

    def to_offer(self):
        return {
            'id': self.id,
            'name': self.name,
            'path': self.path
        }

    def make_facete(self):
        return ':'.join([self.__class__.__name__.lower(),
                         self.path])

    def save(self, **kwargs):
        parent = self.get_parent()
        self.set_position()
        self.validate_slug()
        self.create_path(parent)
        self.set_depth(parent)
        return super(Category, self).save(**kwargs)

    @staticmethod
    def flat_from_tree(tree, drop_id=None):

        def flatify(branch, drop_id):
            if drop_id and not isinstance(drop_id, (list, tuple)):
                drop_id = [drop_id]
            elif not drop_id:
                drop_id = []

            for node in branch:
                if str(node['item'].id) not in drop_id:
                    yield (str(node['item'].id),
                           '{}{}'.format('--' * node['item'].depth,
                                         ' {}'.format(node['item'].name)))
                if len(node['childrens']):
                    if str(node['item'].id) in drop_id:
                        drop_id = [str(c['item'].id) for c in node['childrens']]
                    yield from flatify(node['childrens'], drop_id)

        return flatify(tree, drop_id)

signals.pre_delete.connect(Category.pre_delete, sender=Category)
signals.post_save.connect(Category.post_save, sender=Category)
