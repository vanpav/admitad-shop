import random

from .base import BaseView

from admitad.modules.catalog.models import Offer, Shop, FilterGroup


class OfferView(BaseView):
    template = 'offer.html'

    def dispatch_request(self, slug):
        context = self.get_site_context()

        offer = Offer.objects.get_or_404(slug=slug)
        offer_filters = FilterGroup.get_dict(facets=offer.facets)
        offer_shop = Shop.objects(id=offer.shop.id).first()

        related_offers = Offer.available().filter(id__ne=offer.id,
                                                  id__in=random.sample(
                                                      Offer.available().distinct('id'), 10)
                                                  )

        return self.render_template(offer=offer,
                                    offer_filters=offer_filters,
                                    offer_shop=offer_shop,
                                    related_offers=related_offers,
                                    **context)