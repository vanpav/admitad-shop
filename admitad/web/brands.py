from collections import OrderedDict

from .base import BaseView

from admitad.modules.catalog.models import Brand, Offer
from admitad.ext import cache


class BrandsView(BaseView):
    template = 'brands.html'

    @cache.cached(60)
    def dispatch_request(self):
        context = self.get_site_context()

        pipeline = [
            {
                '$group': {
                    '_id': {
                        'slug': '$brand.slug',
                    },
                    'count': {
                        '$sum': 1
                    }
                }
            },
            {
                '$project': {
                    '_id': 0,
                    'slug': '$_id.slug',
                    'count': 1
                }
            }
        ]

        res = {item['slug']: item['count'] for item in Offer.available().aggregate(*pipeline)}

        brands = Brand.objects(id__in=Offer.available().distinct('brand.id'))
        grouped_brands = {}
        for brand in brands:
            letter = brand.name[:1].upper()
            grouped_brands.setdefault(letter, []).append(
                (brand, res.get(brand.slug, 0))
            )
        grouped_brands = OrderedDict(sorted(grouped_brands.items(), key=lambda x: x[0]))

        return self.render_template(total_brands=brands.count(),
                                    grouped_brands=grouped_brands,
                                    **context)