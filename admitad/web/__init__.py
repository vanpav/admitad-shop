import random
import os

from flask import (Blueprint, render_template, request,
                   redirect, abort, send_from_directory)

from admitad.utils.filesys import check_file_exists, get_full_path
from admitad.modules.catalog.models import (Offer, Filter)

from .base import BaseView
from .offer import OfferView
from .brands import BrandsView
from .catalog import SalesView, CatalogView, FavoritesView

site = Blueprint('site', __name__)


def check_request_args():
    if len(request.args):
        return True
    return False


def get_context():
    return BaseView.get_site_context()


@site.route('/r/<id>/')
def offer_redirect(id):
    offer = Offer.objects.get_or_404(id=id)
    return redirect(offer.url)


@site.route('/')
def index():
    context = get_context()

    try:
        context['offers'] = Offer.available().filter(
            id__in=random.sample(Offer.available().distinct('id'), 10)
        )
    except ValueError:
        context['offers'] = Offer.available()[:10]

    try:
        context['random_tags'] = Filter.objects(
            facete__in=random.sample(Offer.available().distinct('facets'), 25)
        )
    except ValueError:
        context['random_tags'] = Filter.objects(
            facete__in=Offer.available().distinct('facets')
        )
    return render_template('index.html', **context)


site.add_url_rule('/i/<slug>/', view_func=OfferView.as_view('offer'))

site.add_url_rule('/brands/', view_func=BrandsView.as_view('brands'))

site.add_url_rule('/c/sale/', view_func=SalesView.as_view('sale'))

favorites_view = FavoritesView.as_view('favorites')
site.add_url_rule('/favorites/', defaults={'id': None}, view_func=favorites_view)
site.add_url_rule('/favorites/<id>/', view_func=favorites_view)

offers_view = CatalogView.as_view('catalog')
site.add_url_rule('/c/', defaults={'path': None}, view_func=offers_view)
site.add_url_rule('/c/<path:path>/', view_func=offers_view)


@site.errorhandler(404)
def page_not_found(error):
    context = get_context()
    return render_template('errors/404.html', **context), error.code


ALLOWED_EXTENSIONS_TO_SEND = ('png', 'jpg', 'gif', 'ico',
                              'html', 'txt', 'xml')


@site.route('/<path:path>')
def any_route(path):
    _, filename = os.path.split(path)
    _, ext = os.path.splitext(filename)
    if ext.replace('.', '') in ALLOWED_EXTENSIONS_TO_SEND and \
            check_file_exists(get_full_path(os.path.join('static/root', filename))):
        return send_from_directory(
            os.path.join(get_full_path('static/root')), path
        )
    return abort(404)