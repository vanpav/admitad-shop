import datetime

from flask import request, current_app, render_template, session
from flask.views import View
from user_agents import parse

from admitad.modules.catalog.models import (Offer, FilterGroup)


class BaseView(View):

    def __init__(self, template_name=None):
        if template_name:
            self.template = template_name

    def render_template(self, template=None, **kwargs):
        return render_template(template or self.template, **kwargs)

    @staticmethod
    def get_site_context(path=None):
        if request.endpoint in ('site.offer_redirect',):
            return {}

        context = dict(
            current_year=datetime.datetime.now().year,
            site_name=current_app.config.get('SITE_NAME', 'Магазин')
        )

        ua = parse(request.user_agent.string)
        context['is_mobile'] = ua.is_mobile

        context['favorites_count'] = Offer.enabled()\
            .filter(id__in=session.get('favorites', [])).count()

        context['total_offers'] = Offer.available().count()
        context['total_shops'] = len(Offer.available().distinct('shop.id'))
        context['filters'] = FilterGroup.get_dict(
            path=path
        )

        return context