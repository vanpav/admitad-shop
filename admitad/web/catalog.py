import random

from collections import OrderedDict

from flask import request, current_app, redirect, url_for, abort, session, jsonify, Response

from .base import BaseView

from admitad.utils.json import request_wants_json
from admitad.modules.catalog.models import (Offer, Filter, Brand,
                                            Page, Shop, CatalogRedirect)
from admitad.ext import cache


SORTS = {
    'popularity': ('По популярности', '', 1),
    'price_up': ('По возрастанию цены', '+price', 2),
    'price_down': ('По убыванию цены', '-price', 3),
    'discount': ('По размеру скидки', '-discount', 4),
    'new': ('По новинкам', '-created_at', 5)
}

SORTS = OrderedDict(sorted(SORTS.items(), key=lambda k: k[1][2]))

DEFAULT_SORT = 'popularity'


def check_request_args():
    if len(request.args):
        return True
    return False


def make_unique_key(key):

    def inner():
        s = request.path
        s = s + '?' + key + '=' + request.args.get(key, 'null') \
            + '&'.join(k + '=' + v for k,v in request.args.items())
        return s
    return inner


class BaseCatalogView(BaseView):

    model = Offer
    only_fields = ('discount', 'slug', 'pictures', 'id', 'shop', 'name',
                   'price', 'old_price', 'url', 'brand', 'created_at')
    current_brand = None

    @property
    def queryset(self):
        return self.model.available()

    @staticmethod
    def paginate_offers(offers, page):
        return offers.paginate(per_page=36, page=page)

    @property
    def get_page(self):
        try:
            page = int(request.args.get('page', 1))
        except ValueError:
            page = 1
        return page

    @cache.cached(60*60, key_prefix=make_unique_key('brand'))
    def get_brands_and_current(self, oids, current_slug=None):
        brands = []
        current = None
        for brand in Brand.objects(id__in=oids):
            brands.append(brand)
            if brand.slug == current_slug:
                current = brand
        return brands, current

    @cache.cached(60 * 60, key_prefix=make_unique_key('shop'))
    def get_shops_and_current(self, oids, current_slug=None):
        shops = []
        current = None
        for shop in Shop.objects(id__in=oids):
            shops.append(shop)
            if shop.slug == current_slug:
                current = shop
        return shops, current


class CatalogView(BaseCatalogView):
    template = 'catalog.html'
    to_back = ('на ', 'с ', 'со ', 'из ', 'цвета ', 'через ', 'от ')
    current_path = None

    def configure_sorting(self):
        if 'sort' in request.args:
            user_sort = request.args.get('sort', DEFAULT_SORT)
            session['user_sort'] = user_sort
        else:
            user_sort = session.get('user_sort', DEFAULT_SORT)
        self.current_sort = user_sort
        return SORTS.get(user_sort, DEFAULT_SORT)

    def make_query(self, filters):
        return self.queryset.filter(facets__all=sorted(
            [filter.facete for filter in filters])
        )

    def get_current_filters(self):
        if not self.current_path:
            return []
        filter_facets = [filter.replace('_', ':') for filter in self.current_path]
        filters = Filter.objects(facete__in=filter_facets).order_by('+group__slug')
        return filters

    def create_header(self, *filters):
        to_back = []
        to_front = []

        def check(item):
            return item.name.lower().startswith(self.to_back)

        for f in filters:
            if check(f):
                to_back.append(f.name)
            else:
                to_front.append(f.name)
        return lambda header: ' '.join(to_front + [header] + to_back).capitalize()

    def extra_context(self, filters, path):
        context = self.get_site_context(path)
        context['current_filter_groups'] = [f.group.slug for f in filters]
        context['catalog_header'] = self.create_header(*filters)
        context['create_header'] = self.create_header
        return context

    # def redirect_to_properly_path(self, path):
    #     if not pathw:
    #         return
    #     current_path = sorted(path.split('/'))
    #
    #     max_filters = current_app.config.get('MAX_FILTERS', 2)
    #     if len(current_path) > max_filters:
    #         current_path = current_path[:max_filters]
    #
    #     if '/'.join(current_path) != path:
    #         return redirect(url_for('site.catalog', path='/'.join(current_path)))
    #
    #     self.current_path = current_path

    def dispatch_request(self, path=None):
        if path:
            current_path = sorted(path.split('/'))

            for p in current_path:
                if p.startswith('brand'):
                    arg, slug = p.split('_')
                    b = Brand.objects(slug=slug).first()
                    kwargs = {}
                    if b:
                        kwargs[arg] = slug
                    return redirect(url_for('site.catalog', **kwargs), code=301)

            max_filters = current_app.config.get('MAX_FILTERS', 2)
            if len(current_path) > max_filters:
                current_path = current_path[:max_filters]

            if '/'.join(current_path) != path:
                return redirect(url_for('site.catalog', path='/'.join(current_path)), code=301)

            self.current_path = current_path

        filters = self.get_current_filters()
        filters_count = filters.count() if filters else 0

        if self.current_path and not filters_count or \
                        self.current_path and len(self.current_path) != filters_count:
            redir = CatalogRedirect.get_path(path)
            if redir:
                return redirect(url_for('site.catalog', path=redir), code=301)

        if self.current_path and len(self.current_path) != filters_count:
            return abort(404)

        context = self.extra_context(filters, path)
        sorting = self.configure_sorting()

        if path:
            offers = self.make_query(filters)
        else:
            offers = self.queryset

        offers = offers.only(*self.only_fields).order_by(sorting[1])

        context['brands'], context['current_brand'] = \
            self.get_brands_and_current(
                oids=offers.distinct('brand.id'),
                current_slug=request.args.get('brand')
            )

        context['shops'], context['current_shop'] = \
            self.get_shops_and_current(
                oids=offers.distinct('shop.id'),
                current_slug=request.args.get('shop')
            )

        if context.get('current_brand'):
            offers = offers.filter(brand__slug=context.get('current_brand').slug)

        if context.get('current_shop'):
            offers = offers.filter(shop__slug=context.get('current_shop').slug)

        if not offers.count():
            context['filt_offers'] = []
            for i, filt in enumerate(filters):
                os_ids = self.make_query([filt]).distinct('id')
                if len(os_ids) >= 1:
                    os_ids = random.sample(os_ids, 6) if len(os_ids) >= 6 else random.sample(os_ids, len(os_ids))
                    os = self.queryset.filter(id__in=os_ids)
                    context['filt_offers'].append((filt, os))

        offers = self.paginate_offers(offers, self.get_page)

        return self.render_template(self.template,
                                    offers=offers,
                                    current_path=path,
                                    current_sort=self.current_sort,
                                    page=Page.objects(path=path).first(),
                                    sorts=SORTS,
                                    **context)


class SalesView(BaseCatalogView):
    template = 'sale.html'

    def dispatch_request(self):
        context = self.get_site_context()

        offers = self.queryset.filter(discount__gt=0)\
            .order_by('-discount').only(*self.only_fields)

        context['brands'], context['current_brand'] = \
            self.get_brands_and_current(
                oids=offers.distinct('brand.id'),
                current_slug=request.args.get('brand')
            )

        context['shops'], context['current_shop'] = \
            self.get_shops_and_current(
                oids=offers.distinct('shop.id'),
                current_slug=request.args.get('shop')
            )

        if context.get('current_brand'):
            offers = offers.filter(brand__slug=context.get('current_brand').slug)

        if context.get('current_shop'):
            offers = offers.filter(shop__slug=context.get('current_shop').slug)

        offers = self.paginate_offers(offers, self.get_page)

        return self.render_template(self.template,
                                    offers=offers,
                                    **context)


class FavoritesView(BaseCatalogView):
    template = 'favorites.html'

    def dispatch_request(self, id=None):
        favorites = session.get('favorites', []).copy()
        if id:
            Offer.objects.get_or_404(id=id)

            if id in favorites:
                added = False
                favorites.remove(id)
            else:
                added = True
                favorites.append(id)
            session['favorites'] = favorites

            if request_wants_json():
                return jsonify(added=added)
            return redirect(url_for('site.favorites'))

        context = self.get_site_context()
        offers = self.paginate_offers(Offer.enabled(id__in=favorites),
                                      self.get_page)

        return self.render_template(self.template,
                                    offers=offers,
                                    **context)