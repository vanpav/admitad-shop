import os
import random

from flask_script import Manager, Server

from admitad import create_app
from admitad.admin.tasks import indexate_offers
from admitad.modules.catalog.data import populate_data
from admitad.modules.catalog.models import (Offer, Brand, Shop, Filter,
                                            OffersIndex, CatalogRedirect)
from admitad.modules.catalog.models import FilterGroup, Filter
from admitad.modules.auth.models import User

app = create_app('admitad.settings_dev.Config')
manager = Manager(app)

manager.add_command('runserver', Server(
    port=5002,
    use_reloader=True,
    use_debugger=True,
    threaded=True
))


@manager.command
def celery():
    os.system('celery worker -A admitad.celery -l INFO -B')


@manager.command
def clear_tasks():
    from admitad.admin.models import AdminSettings
    settings = AdminSettings.get()
    settings.tasks = {}
    settings.save()


@manager.command
def clear_catalog():
    for cls in (Offer, Brand, Shop, OffersIndex):
        cls.drop_collection()


@manager.command
def populate_filters():

    Filter.objects.delete()
    FilterGroup.objects.delete()

    # Should be better
    # clear_offer_facets()

    for gd in populate_data:
        if isinstance(gd[0], (tuple, list, set)):
            gname, gslug, _ = gd[0]
            init = dict(name=gname, slug=gslug)
        else:
            gname = gd[0]
            init = dict(name=gname)
        group = FilterGroup(**init)
        group.save()
        for fd in gd[1]:
            filt = Filter(name=fd,
                          group=group.to_filter())
            filt.save()


@manager.command
def populate_redirects():
    from pytils.translit import slugify

    def old_slugify(value):
        TRANSTABLE = (
            ('ы', 'y'),
            ('й', 'i'),
            ('я', 'ia'),
            ('х', 'kh'),
            ('ю', 'iu')
        )
        translit = value
        for symb_in, symb_out in TRANSTABLE:
            translit = translit.replace(symb_in, symb_out)

        return slugify(slugify(translit))

    filters = {}

    for gd in populate_data:
        if isinstance(gd[0], (tuple, list, set)):
            gname, gslug, g_old_slug = gd[0]
        else:
            gname = gd[0]
            gslug = slugify(slugify(gname))
            g_old_slug = old_slugify(gname)

        if not filters.get((gslug, g_old_slug)):
            filters[(gslug, g_old_slug)] = []

        for fd in gd[1]:
            filters[(gslug, g_old_slug)].append((slugify(slugify(fd)), old_slugify(fd)))

    filters_items = list(filters.items())

    def p(g, f):
        return '_'.join([g, f])

    def make_paths(filters):

        def inner(slice, depth=1, parent=None):
            paths = []

            for group, filts in slice:
                new_group, old_group = group
                for filt in filts:
                    new_filter, old_filter = filt
                    if parent:
                        new_parent, old_parent = parent
                        if new_filter == new_parent or new_group.split('_')[0] == new_parent.split('_')[0]:
                            continue
                    new_path = '/'.join(sorted([new_parent] + [p(new_group, new_filter)])) \
                        if parent else p(new_group, new_filter)
                    old_path = '/'.join(sorted([old_parent] + [p(old_group, old_filter)])) \
                        if parent else p(old_group, old_filter)
                    paths.append((new_path, old_path))
                    if depth < 2:
                        paths = paths + inner(slice[1:], depth=depth + 1, parent=(new_path, old_path))

            return paths

        return inner(filters)

    paths = make_paths(filters_items)
    for new_path, old_path in paths:
        redirect = CatalogRedirect.create_or_update(old_path, new_path)


@manager.command
def clear_offer_facets():
    offers = Offer.objects(facets__not__size=0)
    offers.update(set__facets=[],
                  set__status__is_filled=False,
                  set__status__is_skipped=False)


@manager.command
def random_filters():
    groups = {}
    for filt in Filter.objects:
        if filt.group.id in groups:
            groups[filt.group.id].append(filt.facete)
        else:
            groups[filt.group.id] = [filt.facete]
    filters = [group for group in groups.values()]

    def make_choice():
        result = []
        for g in filters:
            rnd = random.randint(1, 3)
            choices = random.sample(g.copy(), rnd)
            for f in choices:
                result.append(f)
        return result

    for offer in Offer.objects(facets__size=0):
        choices = make_choice()
        offer.update(set__facets=choices)

    indexate_offers()


@manager.command
def create_xls():
    import xlsxwriter
    from urllib.parse import urljoin
    from flask import url_for, request

    root = request.url_root
    words_to_back = ('на ', 'с ', 'со ', 'из ', 'цвета ', 'через ', 'от ')

    workbook = xlsxwriter.Workbook('{base}/static/root/something.xlsx'.format(base=app.config.get('BASE_DIR')))
    worksheet = workbook.add_worksheet()

    def create_header(*filters):
        to_back = []
        to_front = []

        def check(item):
            return item.name.lower().startswith(words_to_back)

        for f in filters:
            if check(f):
                to_back.append(f.name)
            else:
                to_front.append(f.name)
        return lambda header: ' '.join(to_front + [header] + to_back).capitalize()

    all_filters = Filter.objects()
    for num, index in enumerate(OffersIndex.objects(count__gt=0).order_by('+path')):
        slugs = index.path.replace('_', ':').split('/')
        current_filters = list(all_filters.filter(facete__in=slugs))

        worksheet.write('A{}'.format(num), create_header(*current_filters)('ботильоны'))
        worksheet.write('B{}'.format(num), urljoin(root, url_for('site.catalog', path=index.path)))


    workbook.close()


@manager.command
def create_user(email, password, admin=False):
    user = User.create(email, password, is_admin=admin)

if __name__ == '__main__':
    manager.run()
