let gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    cssmin = require('gulp-csso'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber'),
    uglify = require('gulp-uglify'),
    autoprefixer = require('autoprefixer-stylus');

let paths = {
    src: {
        styl:   'src/styl/',
        js:     'src/js/'
    },
    dist: {
        css:    'admitad/static/css/',
        js:     'admitad/static/js/'
    }
};

gulp.task('default', () => gulp.start([
    'styles',
    'adminStyles',
    'compress',
    'watch'
]));

gulp.task('watch', () => {
    gulp.watch(paths.src.styl + '**/**', () => gulp.start(['styles', 'adminStyles']));
    gulp.watch(paths.src.js + '**/**', () => gulp.start(['compress']));
});

gulp.task('styles', () =>
    gulp.src(paths.src.styl + 'index.styl')
        .pipe(plumber())
        .pipe(stylus({
            use: [autoprefixer()]
        }))
        .pipe(cssmin())
        .pipe(rename('styles.css'))
        .pipe(gulp.dest(paths.dist.css))
);

gulp.task('adminStyles', () =>
    gulp.src(paths.src.styl + 'admin.styl')
        .pipe(plumber())
        .pipe(stylus({
            use: [autoprefixer()]
        }))
        .pipe(cssmin())
        .pipe(rename('admin.css'))
        .pipe(gulp.dest(paths.dist.css))
);

gulp.task('compress', () =>
    gulp.src(paths.src.js + '*.js')
        .pipe(plumber())
        .pipe(uglify())
        .pipe(gulp.dest(paths.dist.js))
);